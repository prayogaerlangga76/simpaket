@extends('layouts.main')

@section('content')
    @php

    $op = 'pegawai';
    if (isset($_GET['op'])) {
        $op = $_GET['op'];
    }

    @endphp

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="mt-5 card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class=" d-flex col-md-5 justify-content-end">
                                <img src="{{ asset('assets\img\logo-assyifa-nobg.png') }}"
                                    style="width:100px; height: 100px">
                            </div>

                            <div class="col-md-5">
                                <br>
                                <h5 class="font-weight-bold text-secondary">LAYANAN PAKET</h5>
                                <h5 class="font-weight-bold text-secondary">AS-SYIFA AL-KHOERIYAH</h5>
                            </div>
                        </div>
                        <div class="container mt-2 border border-secondary pb-5">
                            <div class="row border-bottom">
                                <div class="col-md-6 ">
                                    <h6 class="font-weight-bold mt-4">{{ $judulForm }} Paket</h6>
                                    <h6 class="font-weight-bold">Lokasi : {{ $lokasi }}</h6>

                                </div>
                                <div class="col-md-6 text-right">
                                    <h6 class="badge badge-primary mt-4 text-right">{{ $waktu }}</h6>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <form class="mt-4" action="/penerimaanPaket" method="post">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-2">Paket</label>
                                                <div class="col-md-10">
                                                    <div class="form-check form-check-inline mb-2">
                                                        <input class="form-check-input" type="radio"
                                                            onclick="show_form('pegawai')" name="k_pegawai"
                                                            id="inlineRadio1" value="pegawai"
                                                            @if ($op === 'pegawai') {{ 'checked' }} @endif>
                                                        <label class="form-check-label btn btn-outline-secondary"
                                                            for="inlineRadio1">Pegawai</label>
                                                    </div>
                                                    <div class="form-check form-check-inline mb-2">
                                                        <input class="form-check-input" type="radio"
                                                            onclick="show_form('siswa')" name="k_siswa" id="inlineRadio2"
                                                            value="siswa"
                                                            @if ($op === 'siswa') {{ 'checked' }} @endif>
                                                        <label class="form-check-label btn btn-outline-secondary"
                                                            for="inlineRadio2">Murid</label>
                                                    </div>
                                                    <div class="form-check form-check-inline mb-2">
                                                        <input class="form-check-input" type="radio" name="k_keluarga"
                                                            onclick="show_form('keluarga')" id="inlineRadio3"
                                                            value="Keluarga Pegawai"
                                                            @if ($op === 'keluarga') {{ 'checked' }} @endif>
                                                        <label class="form-check-label btn btn-outline-secondary"
                                                            for="inlineRadio3">Keluarga Pegawai</label>
                                                    </div>
                                                    <div class="form-check form-check-inline mb-2">
                                                        <input class="form-check-input" type="radio" name="k_ltiq"
                                                            onclick="show_form('ltiq')" id="inlineRadio4" value="Ltiq"
                                                            @if ($op === 'ltiq') {{ 'checked' }} @endif>
                                                        <label class="form-check-label btn btn-outline-secondary"
                                                            for="inlineRadio4">Ltiq</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="no_resi" class="col-sm-4 col-form-label">No. Resi</label>
                                                <div class="col-md-8">
                                                    <input type="text"
                                                        class="form-control  @error('no_resi') is-invalid @enderror"
                                                        name="no_resi" id="no_resi">
                                                    @error('no_resi')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if ($op === 'keluarga')
                                                <div class="form-group row">
                                                    <label for="keterangan" class="col-sm-4 col-form-label">Nama
                                                        Keluarga</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="keterangan" id="keterangan"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($op === 'ltiq')
                                                <div class="form-group row">
                                                    <label for="m_ltiq" class="col-sm-4 col-form-label">Nama Santri</label>
                                                    <input type="hidden" name="lokasi" id="lokasi"
                                                        value="{{ $lokasi }}">
                                                    <input type="hidden" name="waktu" id="waktu"
                                                        value="{{ $waktu }}">
                                                    <input type="hidden" name="nama_penerima" id="nama_penerima">
                                                    <input type="hidden" name="id_pegawai" id="id_pegawai">
                                                    <input type="hidden" name="id_telegram" id="id_telegram">

                                                    <div class="col-md-8">
                                                        <select name="m_ltiq" id="m_ltiq"
                                                            class="form-control js-example-basic-single"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="unit" class="col-sm-4 col-form-label">Unit</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('unit') is-invalid @enderror"
                                                            name="unit" id="unit" readonly>
                                                        @error('unit')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="nama_wali"
                                                        class="col-sm-4 col-form-label">Musyrif/ah</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('nama_wali') is-invalid @enderror"
                                                            name="nama_wali" id="nama_wali" readonly>
                                                        @error('nama_wali')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($op === 'pegawai' || $op === 'keluarga')
                                                <div class="form-group row">
                                                    <label for="nama_penerima_pegawai" class="col-sm-4 col-form-label">Nama
                                                        Pegawai</label>
                                                    <input type="hidden" name="lokasi" id="lokasi"
                                                        value="{{ $lokasi }}">
                                                    <input type="hidden" name="waktu" id="waktu"
                                                        value="{{ $waktu }}">
                                                    <input type="hidden" name="id_telegram" id="id_telegram">
                                                    <input type="hidden" name="paket_nama" id="paket_nama">
                                                    <div class="col-md-8">
                                                        <select
                                                            class="form-control js-example-data-array @error('pegawai_id') is-invalid @enderror"
                                                            id="nama_penerima_pegawai" name="pegawai_id"></select>
                                                        @error('pegawai_id')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($op === 'siswa')
                                                <div class="form-group row">
                                                    <label for="nama_penerima_pegawai" class="col-sm-4 col-form-label">Nama
                                                        Penerima</label>
                                                    <input type="hidden" name="lokasi" id="lokasi"
                                                        value="{{ $lokasi }}">
                                                    <input type="hidden" name="waktu" id="waktu"
                                                        value="{{ $waktu }}">
                                                    <input type="hidden" name="id_telegram" id="id_telegram">
                                                    <input type="hidden" name="id_pegawai" id="id_pegawai">
                                                    <input type="hidden" name="paket_nama" id="paket_nama">
                                                    <div class="col-md-8">
                                                        <select
                                                            class="form-control js-example-data-array @error('nis') is-invalid @enderror"
                                                            id="nama_penerima_murid" name="nis">
                                                            @error('nis')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($op === 'pegawai' || $op === 'keluarga')
                                                <div class="form-group row">
                                                    <label for="bidang" class="col-sm-4 col-form-label">Bidang</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('bidang') is-invalid @enderror"
                                                            name="bidang" id="bidang" readonly>
                                                        @error('bidang')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="unit" class="col-sm-4 col-form-label ">Unit</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('unit') is-invalid @enderror"
                                                            name="unit" id="unit" readonly>
                                                        @error('unit')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($op === 'siswa')
                                                <div class="form-group row">
                                                    <label for="unit" class="col-sm-4 col-form-label ">Unit</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('unit') is-invalid @enderror"
                                                            name="unit" id="unit" readonly>
                                                        @error('unit')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="no_kamar" class="col-sm-4 col-form-label ">No. Kamar</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('no_kamar') is-invalid @enderror"
                                                            name="no_kamar" id="no_kamar" readonly>
                                                        @error('no_kamar')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="nama_wali"
                                                        class="col-sm-4 col-form-label ">Musyrif/ah</label>
                                                    <div class="col-md-8">
                                                        <input type="text"
                                                            class="form-control @error('nama_wali') is-invalid @enderror"
                                                            name="nama_wali" id="nama_wali" readonly>
                                                        @error('nama_wali')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="ekspedisi_id" class="col-md-4">Kurir</label>
                                                <div class="col-md-8">
                                                    @foreach ($ekspedisi as $e)
                                                        <div class="form-check form-check-inline mb-2">
                                                            <input class="form-check-input" type="radio" name="ekspedisi_id"
                                                                id="Radio{{ $loop->iteration }}"
                                                                value="{{ $e->id }}">
                                                            <label class="form-check-label btn btn-outline-secondary"
                                                                for="Radio{{ $loop->iteration }}">{{ $e->name }}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama_pengirim" class="col-sm-4 col-form-label">Nama
                                                    Pengirim</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="nama_pengirim" id="nama_pengirim"
                                                        class="form-control ">
                                                </div>
                                            </div>


                                            <button type="submit" class="btn btn-primary container">Save</button>
                                            <a href="/penerimaan"
                                                class="btn btn-secondary mt-2 d-flex justify-content-center">Back</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function show_form(a) {
            window.location.href = "?op=" + a;
        }
    </script>
@endsection
