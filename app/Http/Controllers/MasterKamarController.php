<?php

namespace App\Http\Controllers;

use App\Models\Kamar;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class MasterKamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterKamar.index', [
            'title' => 'Daftar Kamar Santri As - syifa',
            'kamar' => Kamar::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterKamar.create', [
            'title' => 'Add new kamar page',
            'pegawai' => Pegawai::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_kamar' => 'required',
            'pegawai_id' => 'required'
        ]);

        Kamar::create($validatedData);

        return redirect('/kamar')->with('success', 'New kamar has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function show(Kamar $kamar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function edit(Kamar $kamar)
    {
        return view('masterKamar.edit', [
            'title' => 'Edit Kamar Page',
            'kamar' => Kamar::findOrFail($kamar->id),
            'pegawai' => Pegawai::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kamar $kamar)
    {
        $validatedData = $request->validate([
            'nama_kamar' => 'required',
            'pegawai_id' => 'required'
        ]);
        Kamar::where('id', $kamar->id)->update($validatedData);
        return redirect('/kamar')->with('success', 'Kamar has been Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kamar $kamar)
    {
        Kamar::destroy('id', $kamar->id);
        return redirect('/kamar')->with('success', 'Kamar has been Deleted!');
    }
}
