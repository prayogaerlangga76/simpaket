<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pj extends Model
{
    use HasFactory;

    protected $table = 'pj';

    protected $guarded = ['id'];


    public function lokasi()
    {
        return $this->belongsTo(Lokasi::class);
    }

    public function cariPj($lokasi)
    {
        $join = Pj::join('lokasi', 'pj.lokasi_id', '=', 'lokasi.id')->where('lokasi.nama_lokasi', $lokasi)->first();
        return [$join['nama_pegawai'], $join['no_telepon'], $join['nama_lokasi']];
    }
}
