<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pegawai.index', [
            'title' => 'Daftar Pegawai',
            'pegawai' => Pegawai::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pegawai.create', [
            'title' => 'Add new pegawai page',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_pegawai' => 'required',
            'bidang' => 'required',
            'unit' => 'required'
        ]);
        $validatedData['user_id'] = 1;
        
        
        Pegawai::create($validatedData);

        return redirect('/pegawai')->with('success', 'New pegawai has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pegawai.edit', [
            'title' => 'Edit pegawai Page',
            'pegawai' => Pegawai::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'nama_pegawai' => 'required',
            'bidang' => 'required',
            'unit' => 'required',
            'user_id' => 'required'
        ]);
        Pegawai::where('id', $id)->update($validatedData);
        return redirect('/pegawai')->with('success', 'Pegawai has been Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pegawai::destroy('id', $id);
        return redirect('/pegawai')->with('success', 'Pegawai has been Deleted!');
    }
}
