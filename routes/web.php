<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PjController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LokasiController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\EkspedisiController;
use App\Http\Controllers\GetdataController;
use App\Http\Controllers\LtiqController;
use App\Http\Controllers\TransaksiPaketController;
use App\Models\TransaksiPaket;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    //
    Route::get('/', [WelcomeController::class, 'index']);

    Route::resource('role', RoleController::class)->except('show');
    Route::resource('ekspedisi', EkspedisiController::class)->except('show');


    Route::get('/penerimaan', [TransaksiPaketController::class, 'index']);

    Route::get('/penerimaan/listltiq', [GetdataController::class, 'listLtiq']);
    Route::get('/penerimaan/showPegawai/{pegawai:id_pegawai}', [TransaksiPaketController::class, 'showPegawai']);
    Route::get('/penerimaan/showPaket/{TransaksiPaket}', [TransaksiPaketController::class, 'showPaket']);
    Route::get('/penerimaan/listPegawai', [TransaksiPaketController::class, 'listPegawai']);
    Route::get('/penerimaan/listMurid', [TransaksiPaketController::class, 'listMurid']);
    Route::get('/penerimaan/listPaket', [TransaksiPaketController::class, 'listPaket']);
    Route::get('/penerimaan/showMurid/{siswa:nis}', [TransaksiPaketController::class, 'showMurid']);
    Route::get('/penerimaan/showLtiq/{ltiq}', [TransaksiPaketController::class, 'showLtiq']);
    Route::get('/penerimaanPaket', [TransaksiPaketController::class, 'createCagak']);
    Route::post('/penerimaanPaket', [TransaksiPaketController::class, 'store']);
    Route::get('/penerimaanWanareja', [TransaksiPaketController::class, 'createWanareja']);
    Route::get('/penerimaanSagalaherang', [TransaksiPaketController::class, 'createSagalaherang']);

    Route::resource('/lokasi', LokasiController::class);
    Route::resource('/pj', PjController::class);
    Route::resource('/ltiq', LtiqController::class);



    Route::get('/pengambilan', [TransaksiPaketController::class, 'indexPengambilan']);
    Route::get('/pengambilanCagak', [TransaksiPaketController::class, 'createPengambilanCagak']);
    Route::get('/pengambilanWanareja', [TransaksiPaketController::class, 'createPengambilanWanareja']);
    Route::get('/pengambilanSagalaherang', [TransaksiPaketController::class, 'createPengambilanSagalaherang']);
    Route::post('/pengambilanPaket', [TransaksiPaketController::class, 'storePengambilan']);
    Route::get('/laporan', [LaporanController::class, 'index']);
    Route::get('/laporanGrafik', [LaporanController::class, 'laporanGrafik']);
    Route::get('/laporanGrafik/sagalaherang', [LaporanController::class, 'grafikSagalaherang']);
    Route::get('/laporanGrafik/wanareja', [LaporanController::class, 'grafikWanareja']);
    Route::get('/laporanGrafik/cagak', [LaporanController::class, 'grafikCagak']);
    Route::get('/laporanGrafik', [LaporanController::class, 'laporanGrafik']);
    Route::get('/laporan/cagak', [LaporanController::class, 'laporanCagak']);
    Route::get('/laporan/wanareja', [LaporanController::class, 'laporanWanareja']);
    Route::get('/laporan/sagalaherang', [LaporanController::class, 'laporanSagalaherang']);
    Route::delete('/laporan/{TransaksiPaket}', [LaporanController::class, 'destroy']);
    Route::put('/profile/{profile:username}', [ProfileController::class, 'updateProfile']);
    Route::get('/profile/{profile:username}/edit', [ProfileController::class, 'profile']);

    Route::get('/user', [UserController::class, 'index']);
    Route::get('/user/{user}/edit', [UserController::class, 'edit']);
    Route::put('/user/{user}', [UserController::class, 'update']);
    Route::delete('/user/{user}', [UserController::class, 'destroy']);
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/user/create', [UserController::class, 'create']);

    Route::get('/logout', [LoginController::class, 'logout']);

    Route::put('/ceklisPaket/{id}', [TransaksiPaketController::class, 'ceklisPaket']);
});

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
