@extends('layouts.main')


@section('content')
    @php
    $periode = '';
    if (isset($_GET['periode'])) {
        $periode = $_GET['periode'];
    }
    @endphp
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <nav class="navbar">
                <h5 class="font-weight-bold text-primary">Laporan Grafik</h5>
                <div class="row mr-5 pr-5"></div>
                <div class="row mr-5 pr-5"></div>
                <div class="row mr-5"></div>
                <div class="row ml-5 pl-4">
                    <a href="/laporanGrafik/cagak"
                        class="btn btn-primary mr-1 mb-2 d-flex justify-content-center">Jalancagak</a>
                    <a href="/laporanGrafik/wanareja"
                        class="btn btn-warning mr-1 d-flex mb-2 justify-content-center">Wanareja</a>
                    <a href="/laporanGrafik/sagalaherang"
                        class="btn btn-danger mr-1 d-flex mb-2 justify-content-center">Sagalaherang</a>
                </div>

                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block">
                        <a href="/laporanGrafik" class="btn btn-primary  d-flex mb-2 justify-content-center">Refresh
                            Data</a>
                    </div>
                </ul>
            </nav>

        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <form action="/laporanGrafik" method="GET">
                            <label for="periode">Periode Laporan</label><br>
                            <div class="input-group mb-3">
                                <select name="periode" id="periode" class="form-control"
                                    onchange="ganti_periode(this.value)">
                                    <option value="">Seluruh Data</option>
                                    @if (request()->periode == '01')
                                        <option value="01" selected>Januari</option>
                                    @else
                                        <option value="01">Januari</option>
                                    @endif
                                    @if (request()->periode == '02')
                                        <option value="02" selected>Febuari</option>
                                    @else
                                        <option value="02">Febuari</option>
                                    @endif
                                    @if (request()->periode == '03')
                                        <option value="03" selected>Maret</option>
                                    @else
                                        <option value="03">Maret</option>
                                    @endif
                                    @if (request()->periode == '04')
                                        <option value="04" selected>April</option>
                                    @else
                                        <option value="04">April</option>
                                    @endif
                                    @if (request()->periode == '05')
                                        <option value="05" selected>Mei</option>
                                    @else
                                        <option value="05">Mei</option>
                                    @endif
                                    @if (request()->periode == '06')
                                        <option value="06" selected>Juni</option>
                                    @else
                                        <option value="06">Juni</option>
                                    @endif
                                    @if (request()->periode == '07')
                                        <option value="07" selected>Juli</option>
                                    @else
                                        <option value="07">Juli</option>
                                    @endif
                                    @if (request()->periode == '08')
                                        <option value="08" selected>Agustus</option>
                                    @else
                                        <option value="08">Agustus</option>
                                    @endif
                                    @if (request()->periode == '09')
                                        <option value="09" selected>September</option>
                                    @else
                                        <option value="09">September</option>
                                    @endif
                                    @if (request()->periode == '10')
                                        <option value="10" selected>Oktober</option>
                                    @else
                                        <option value="10">Oktober</option>
                                    @endif
                                    @if (request()->periode == '11')
                                        <option value="11" selected>November</option>
                                    @else
                                        <option value="11">November</option>
                                    @endif
                                    @if (request()->periode == '12')
                                        <option value="12" selected>Desember</option>
                                    @else
                                        <option value="12">Desember</option>
                                    @endif
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-right">
                        <p>Lokasi : {{ $lokasi }} </p>
                    </div>
                </div>
                <div id="chartWilayah" class="mb-5"></div>
                <div id="chartKategori" class="mb-5"></div>
                <div id="chart" class="mb-5"></div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/exporting.js') }}"></script>
    <script src="{{ asset('assets/js/export-data.js') }}"></script>
    <script type="text/javascript">
        function ganti_periode(periode) {
            document.location.href = '?periode=' + periode;
        }
        var kurir = <?= str_replace("\"", '', json_encode($kurir)) ?>;
        Highcharts.chart('chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Paket per Ekspedisi Tahun ' + <?php echo date('Y'); ?>
            },
            legend: {
                enabled: false
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: 'Ekspedisi'
                }

            },
            plotOptions: {
                series: {
                    allowPointSelect: true,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:,.0f}'
                    }
                },
            },
            series: [{
                name: 'Total Paket',
                colorByPoint: true,
                data: kurir
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
        var kurir = <?= str_replace("\"", '', json_encode($paket)) ?>;
        Highcharts.chart('chartWilayah', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Paket per Wilayah Tahun ' + <?php echo date('Y'); ?>
            },
            legend: {
                // layout: 'vertical',
                // align: 'right',
                // verticalAlign: 'middle'
                enabled: false
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: 'Wilayah'
                }

            },
            plotOptions: {
                series: {
                    allowPointSelect: true,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:,.0f}'
                    }
                },
            },
            series: [{
                name: 'Total Paket',
                colorByPoint: true,
                data: kurir
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
        var kurir = <?= str_replace("\"", '', json_encode($kategori)) ?>;
        Highcharts.chart('chartKategori', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Paket per Kategori Tahun ' + <?php echo date('Y'); ?>
            },
            legend: {
                enabled: false
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: 'Kategori'
                }

            },
            plotOptions: {
                series: {
                    allowPointSelect: true,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:,.0f}'
                    }
                },
            },
            series: [{
                name: 'Total Paket',
                colorByPoint: true,
                data: kurir
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    </script>
@endpush
