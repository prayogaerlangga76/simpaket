<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $table = 'siswa_v';

    protected $connection = 'mysql3';


    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'idkelas', 'kode_kelas');
    }
}
