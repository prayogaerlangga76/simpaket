@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Update username<h1>
                                        <form class="user" action="/user/{{ $user->id }}" method="post">
                                            @method('put')
                                            @csrf
                                            <div class="form-group row">
                                                <h6 for="bidang" class="col-sm-4">Username</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        value="{{ $user->username }}" name="username" id="username">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="email" class="col-sm-4">Email</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" value="{{ $user->email }}"
                                                        name="email" id="email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="password" class="col-sm-4">Ganti Password</h6>
                                                <div class="col-md-8">
                                                    <input type="password" class="form-control" name="password"
                                                        id="password">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="role_id" class="col-sm-4">Role</h6>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="role_id" name="role_id">
                                                        <option selected disabled>-- Pilih Role --</option>
                                                        @foreach ($role as $l)
                                                            @if ($user->role_id == $l->id)
                                                                <option value="{{ $l->id }}" selected>
                                                                    {{ $l->name }}</option>
                                                            @else
                                                                <option value="{{ $l->id }}">{{ $l->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="lokasi_id" class="col-sm-4">Lokasi</h6>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="lokasi_id" name="lokasi_id">
                                                        <option selected disabled>-- Pilih Lokasi --</option>
                                                        @foreach ($lokasi as $l)
                                                            @if ($user->lokasi_id == $l->id)
                                                                <option value="{{ $l->id }}" selected>
                                                                    {{ $l->nama_lokasi }}</option>
                                                            @else
                                                                <option value="{{ $l->id }}">{{ $l->nama_lokasi }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" class="pegawai_id" name="pegawai_id">
                                            <div class="form-group row">
                                                <h6 for="pegawai_id" class="col-sm-4">Pegawai</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        value="{{ $user->pegawai->nama_pegawai }}" name="nama_pegawai"
                                                        id="nama_pegawai" readonly>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary mr-2">Save</button>
                                            <a href="/user" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
