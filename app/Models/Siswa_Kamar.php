<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa_Kamar extends Model
{
    use HasFactory;

    protected $table = 'siswa_kamar';

    protected $guarded = ['id'];

    public function mkamar()
    {
        return $this->belongsTo(Kamar::class, 'kamar_id');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class, 'nis');
    }
}
