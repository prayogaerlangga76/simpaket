<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JoinSiswa extends Model
{
    use HasFactory;

    protected $table = 'siswa_kamar_v';
    protected $connection = 'mysql4';
}
