@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                <div class="mt-5 card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class=" d-flex col-md-5 justify-content-end">
                                <img src="{{ asset('assets\img\logo-assyifa-nobg.png') }}"
                                    style="width:100px; height: 100px">
                            </div>
                            <div class="col-md-5">
                                <br>
                                <h5 class="font-weight-bold text-secondary">LAYANAN PAKET</h5>
                                <h5 class="font-weight-bold text-secondary">AS-SYIFA AL-KHOERIYAH</h5>
                            </div>
                        </div>
                        <div class="container text-center mt-2 border pb-5">
                            <h6 class="text-center font-weight-bold mt-4">{{ $judulForm }} Paket</h6>
                            <h6 class="text-center badge badge-primary">{{ $waktu }}</h6>
                            <div class="container text-center">
                                <h6 class="text-center font-weight-bold my-3">Pilih lokasi</h6>
                                <div class="row">
                                    @if (Auth::user()->pegawai->unit == 'DEPT. IT' && Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                        @if ($judulForm == 'Penerimaan')
                                            <div class="col-md-4">
                                                <a href="/penerimaanPaket"
                                                    class="btn btn-primary mb-2 d-flex justify-content-center">Jalancagak</a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="/penerimaanWanareja"
                                                    class="btn btn-warning d-flex mb-2 justify-content-center">Wanareja</a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="/penerimaanSagalaherang"
                                                    class="btn btn-danger d-flex mb-2 justify-content-center">Sagalaherang</a>
                                            </div>
                                        @else
                                            <div class="col-md-4">
                                                <a href="/pengambilanCagak"
                                                    class="btn btn-primary mb-2 d-flex justify-content-center">Jalancagak</a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="/pengambilanWanareja"
                                                    class="btn btn-warning d-flex mb-2 justify-content-center">Wanareja</a>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="/pengambilanSagalaherang"
                                                    class="btn btn-danger d-flex mb-2 justify-content-center">Sagalaherang</a>
                                            </div>
                                        @endif
                                    @else
                                        @if ($judulForm == 'Penerimaan')
                                            @if (Auth::user()->lokasi_id == 1)
                                                <div class="col-md-4">
                                                    <a href="/penerimaanPaket"
                                                        class="btn btn-primary mb-2 d-flex justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif

                                            @if (Auth::user()->lokasi_id == 2)
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="/penerimaanWanareja"
                                                        class="btn btn-primary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif
                                            @if (Auth::user()->lokasi_id == 3)
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="/penerimaanSagalaherang"
                                                        class="btn btn-primary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif
                                        @else
                                            @if (Auth::user()->lokasi_id == 1)
                                                <div class="col-md-4">
                                                    <a href="/pengambilanCagak"
                                                        class="btn btn-primary mb-2 d-flex justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif
                                            @if (Auth::user()->lokasi_id == 2)
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="/pengambilanWanareja"
                                                        class="btn btn-primary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif
                                            @if (Auth::user()->lokasi_id == 3)
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Jalancagak</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#"
                                                        class="btn btn-secondary d-flex mb-2 justify-content-center">Wanareja</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="/pengambilanSagalaherang"
                                                        class="btn btn-primary d-flex mb-2 justify-content-center">Sagalaherang</a>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
