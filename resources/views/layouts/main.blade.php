<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }}</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/font_apis.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/select2-bootstrap.min.css') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/img/logo-humas.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/logo-humas.png') }}">
    <script src="{{ asset('assets/js/f2079d386e.js') }}" crossorigin="anonymous"></script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.partials.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('sweetalert::alert')

                @include('layouts.partials.topbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Simpaket As - Syifa 2022</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sudah Selesai ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Pastikan semua pekerjaan sudah diperiksa sebelum meninggalkan sistem.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/highcharts.js') }}"></script>

    
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                responsive: true,
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#dataTable2').DataTable({
                dom: "<'row mt-4', <'col-md-4'f> "+"<'col-md-4'l>"+"<'col-md-4'B> <'container'tip>>",
                buttons: [
                    'excel', 'pdf', 'print'
                ],
                responsive: true,
            });
        });
    </script>

    <script>
        $(document).on('change', '#nama_penerima_pegawai', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPegawai/" + $('#nama_penerima_pegawai').val(),
                dataType: "json",
                success: function(response) {
                    $('#bidang').val(response.bidang);
                    $('#unit').val(response.unit);
                    $('#paket_nama').val(response.nama_pegawai);
                    $('#id_telegram').val(response.id_telegram);
                }

            });
        });
        $(document).on('change', '#m_ltiq', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showLtiq/" + $('#m_ltiq').val(),
                dataType: "json",
                success: function(response) {
                    $('#nama_penerima').val(response.nama);
                    $('#id_pegawai').val(response.pegawai_id);
                    $('#unit').val(response.unit);
                    $('#nama_wali').val(response.nama_pegawai);
                    $('#id_telegram').val(response.id_telegram);
                }

            });
        });
        $(document).on('change', '#pegawai_id', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPegawai/" + $('#pegawai_id').val(),
                dataType: "json",
                success: function(response) {
                    $('.pegawai_id').val(response.id_pegawai);
                }

            });
        });
        $(document).on('change', '#nama_penerima_murid', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showMurid/" + $('#nama_penerima_murid').val(),
                dataType: "json",
                success: function(response) {
                    $('#nama_wali').val(response.nama_pegawai);
                    $('#id_telegram').val(response.id_telegram);
                    $('#paket_nama').val(response.nama);
                    $('#no_kamar').val(response.nama_kamar);
                    $('#unit').val(response.nama_asrama);
                    $('#id_pegawai').val(response.id_pegawai);
                }

            });
        });
        $(document).on('change', '#js-transaksi-data', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPaket/" + $('.js-transaksi-data').val(),
                dataType: "json",
                success: function(response) {
                    $('#kelompok').val(response.kelompok);
                    $('#nama_penerima').val(response.nama_penerima);
                    $('#bidang').val(response.bidang);
                    $('#unit').val(response.unit);
                    $('#no_kamar').val(response.no_kamar);
                    $('#keterangan').val(response.keterangan);
                    $('#nama_pengirim').val(response.nama_pengirim);
                    $('#id').val(response.id);
                    $('#j_kurir').val(response.ekspedisi_id);
                    $('#id_telegram').val(response.id_telegram);
                    $('#no_uniq').val(response.no_resi);
                }

            });
        });
        $("input[type=checkbox]").on("change", function(e) {
                    e.preventDefault();
                    var id = $(this).val();
                    var n = $("input:checked").length;
                    var data = {
                        'status': n
                    }
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "PUT",
                        url: "{{ url('ceklisPaket') }}/" + id,
                        data: data,
                        dataType: "json",
                        success: function(response) {
                            $('.pesan').html("");
                            $('.pesan').addClass('alert alert-success');
                            $('.pesan').text(response.message);
                            setTimeout(() => {
                                $('.pesan').html("");
                                $('.pesan').removeClass(
                                    'alert alert-success');
                            }, 5000);
                            location.reload(true);
                        }
                    });
                });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#m_ltiq').select2({
                minimumInputLength: 3,
                theme: "bootstrap",
                allowClear: true,
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listltiq',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#nama_musyrif').select2({
                minimumInputLength: 3,
                theme: "bootstrap",
                allowClear: true,
                placeholder: 'Masukan Nama Musyrif',
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listPegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
    </script>
    <script type=text/javascript>
        $(function() {
            $('#nama_penerima_pegawai').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan Nama Penerima',
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listPegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
        $(function() {
            $('#pegawai_id').select2({
                minimumInputLength: 3,
                theme: "bootstrap",
                allowClear: true,
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listPegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
        $(function() {
            $('.js-transaksi-data').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan No Resi',
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listPaket',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-transaksi-data option:selected").text();
            });
        });
        $(function() {
            $('#nama_penerima_murid').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan Nama Penerima',
                ajax: {
                    dataType: 'json',
                    url: '/penerimaan/listMurid',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
    </script>
    @stack('script')
</body>

</html>
