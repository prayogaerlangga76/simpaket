<?php

namespace App\Http\Controllers;

use App\Models\Pj;
use Carbon\Carbon;
use App\Models\Ltiq;
use App\Models\Kelas;
use App\Models\Siswa;
use GuzzleHttp\Client;
use App\Models\Pegawai;
use App\Models\Ekspedisi;
use App\Models\JoinSiswa;
use Illuminate\Http\Request;
use App\Models\TransaksiPaket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;



class TransaksiPaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * return \Illuminate\Http\Response
     */
    public function index()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        return view('transaksi.index', [
            'title' => 'Penerimaan Paket',
            'waktu' => $waktu,
            'judulForm' => 'Penerimaan'
        ]);
    }
    public function indexPengambilan()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        return view('transaksi.index', [
            'title' => 'Pengambilan Paket',
            'waktu' => $waktu,
            'judulForm' => 'Pengambilan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * return \Illuminate\Http\Response
     */

    public function showPegawai(Pegawai $pegawai)
    {
        $cek = Pegawai::where('id_pegawai', $pegawai->id_pegawai)->first();
        return response()->json([
            'nama_pegawai' => $cek['nama_pegawai'],
            'bidang' => $cek['bidang'],
            'unit' => $cek['unit'],
            'id_telegram' => $cek['id_telegram'],
            'id_pegawai' => $cek['id_pegawai'],
        ]);
    }
    public function showPaket(TransaksiPaket $TransaksiPaket)
    {

        $cek = TransaksiPaket::where('id', $TransaksiPaket->id)->first();
        if ($cek['pegawai_id'] != null) {
            return response()->json([
                'id' => $cek['id'],
                'no_resi' => $cek['no_resi'],
                'id_telegram' => $cek->pegawai->id_telegram,
                'nama_pengirim' => $cek['nama_pengirim'],
                'nama_penerima' => $cek['nama_penerima'],
                'kelompok' => $cek['kelompok'],
                'bidang' => $cek['bidang'],
                'unit' => $cek['unit'],
                'no_kamar' => $cek['no_kamar'],
                'nama_wali' => $cek['nama_wali'],
                'keterangan' => $cek['keterangan'],
                'ekspedisi_id' => $cek->ekspedisi->name,
            ]);
        } else {
            return response()->json([
                'id' => $cek['id'],
                'no_resi' => $cek['no_resi'],
                'nama_pengirim' => $cek['nama_pengirim'],
                'nama_penerima' => $cek['nama_penerima'],
                'kelompok' => $cek['kelompok'],
                'bidang' => $cek['bidang'],
                'unit' => $cek['unit'],
                'no_kamar' => $cek['no_kamar'],
                'nama_wali' => $cek['nama_wali'],
                'keterangan' => $cek['keterangan'],
                'ekspedisi_id' => $cek->ekspedisi->name,
            ]);
        }
    }
    public function listPegawai(Request $request)
    {
        

        $cek = Pegawai::select('id_pegawai as id', 'nama_pegawai as text')->where('nama_pegawai', 'like', "%$request[search]%")->get();
        return response()->json($cek);
    }


    public function listMurid(Request $request)
    {
        $db_kelas = (new Kelas())->getConnection()->getDatabaseName();
        $tb_kelas = (new Kelas())->getTable();
        $tb_siswa = (new Siswa())->getTable();
        $cek = Siswa::select('nis as id', 'nama as text')->join($db_kelas . '.' . $tb_kelas, function ($join) use ($db_kelas, $tb_kelas, $tb_siswa) {
            $join->on($db_kelas . '.' . $tb_kelas . '.kode_kelas', $tb_siswa . '.idkelas');
        })->where('smp_smu_aktif', 'YA')->where('nama', 'like', '%' . $request['search'] . '%')->where($tb_kelas . '.kampus', strtoupper(Auth::user()->lokasi->nama_lokasi))->get();
        return response()->json($cek);
    }
    public function listPaket(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            $cek = TransaksiPaket::select('id', 'no_resi as text')->Where('no_resi', 'like', "%$request[search]%")->where('lokasi', Auth::user()->lokasi->nama_lokasi)->get();
        } else {
            $cek = TransaksiPaket::select('id', 'no_resi as text')->Where('no_resi', 'like', "%$request[search]%")->get();
        }
        return response()->json($cek);
    }
    public function showPegawaiPengambilan(Pegawai $pegawai)
    {
        $cek = Pegawai::where('id_pegawai', $pegawai->id_pegawai)->first();
        return response()->json([
            'nama_pegawai' => $cek['nama_pegawai'],
            'bidang' => $cek['bidang'],
            'unit' => $cek['unit'],
            'id_telegram' => $cek['id_telegram'],
        ]);
    }

    public function showLtiq(Ltiq $ltiq)
    {
        $cek = Ltiq::where('id', $ltiq->id)->first();
        return response()->json([
            'nama' => $cek['nama'],
            'id' => $cek['id'],
            'pegawai_id' => $cek['pegawai_id'],
            'nama_pegawai' => $cek->pegawai->nama_pegawai,
            'id_telegram' => $cek->pegawai->id_telegram,
            'unit' => $cek['unit'],
        ]);
    }
    public function showMurid(Siswa $siswa)
    {
        $db_pegawai = (new Pegawai())->getConnection()->getDatabaseName();
        $tb_pegawai = (new Pegawai())->getTable();
        $tb_siswa = (new JoinSiswa())->getTable();
        $cek = JoinSiswa::join($db_pegawai . '.' . $tb_pegawai, function ($join) use ($db_pegawai, $tb_pegawai, $tb_siswa) {
            $join->on($db_pegawai . '.' . $tb_pegawai . '.id_pegawai', $tb_siswa . '.wali_kamar');
        })->where('id_siswa', $siswa->nis)->first();
        return response()->json([
            'nama_pegawai' => $cek['nama_pegawai'],
            'id_telegram' => $cek['id_telegram'],
            'nama_kamar' => $cek['nama_kamar'],
            'nama_asrama' => $cek['nama_asrama'],
            'nama' => $cek['nama'],
            'id_pegawai' => $cek['id_pegawai'],
        ]);
    }

    public function createCagak()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        $pegawai = Pegawai::all();
        $ekspedisi = Ekspedisi::all();
        return view('transaksi.penerimaanForm', [
            'lokasi' => 'Jalancagak',
            'waktu' => $waktu,
            'title' => 'SIMPAKET | Jalancagak',
            'judulForm' => 'Penerimaan',
            'pegawai' => $pegawai,
            'ekspedisi' => $ekspedisi,
        ]);
    }

    public function ceklisPaket(Request $request, $id){
        $validasi = $request->validate([
            'status' => 'required'
        ]);

        $data = TransaksiPaket::find($id);
        $data->status = $validasi['status'];
        $data->nama_pengambil = $data->nama_penerima;
        $data->user_id = Auth::user()->id;
        $data->tgl_ambil = date('Y-m-d H:i:s');
        $data->update();

        return response()->json([
            'status' => 200,
            'message' => 'Data sudah diupdate'
        ]);

    }
    public function createWanareja()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        $pegawai = Pegawai::all();
        $ekspedisi = Ekspedisi::all();
        return view('transaksi.penerimaanForm', [
            'lokasi' => 'Wanareja',
            'waktu' => $waktu,
            'title' => 'SIMPAKET | Wanareja',
            'judulForm' => 'Penerimaan',
            'pegawai' => $pegawai,
            'ekspedisi' => $ekspedisi,
        ]);
    }
    public function createSagalaherang()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        $pegawai = Pegawai::all();
        $ekspedisi = Ekspedisi::all();
        return view('transaksi.penerimaanForm', [
            'lokasi' => 'Sagalaherang',
            'waktu' => $waktu,
            'title' => 'SIMPAKET | Sagalaherang',
            'judulForm' => 'Penerimaan',
            'pegawai' => $pegawai,
            'ekspedisi' => $ekspedisi,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * param  \Illuminate\Http\Request  $request
     * return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request['k_pegawai'] && $request['lokasi']) {
            $validatedData = $request->validate([
                'no_resi' => 'required|unique:transaksi_paket',
                'pegawai_id' => 'required',
                'ekspedisi_id' => 'required',
                'lokasi' => 'required',
                'k_pegawai' => 'required',
                'nama_pengirim' => 'required',
                'paket_nama' => 'required',
                'unit' => 'required',
                'bidang' => 'required'
            ]);

            $data = new TransaksiPaket();
            $data->no_resi = $validatedData['no_resi'];
            $data->pegawai_id = $validatedData['pegawai_id'];
            $data->ekspedisi_id = $validatedData['ekspedisi_id'];
            $data->nama_penerima = $validatedData['paket_nama'];
            $data->unit = $validatedData['unit'];
            $data->bidang = $validatedData['bidang'];
            $data->lokasi = $validatedData['lokasi'];
            $data->kelompok = $validatedData['k_pegawai'];
            $data->tgl_terima = date('Y-m-d H:i:s');
            $data->nama_pengirim = $validatedData['nama_pengirim'];

            $pj = Pj::cariPj($validatedData['lokasi']);
            $kurir = TransaksiPaket::kurir($validatedData['ekspedisi_id']);
            $pesan = "*[Informasi Penerimaan Paket]*
            
Ykh. Ustadz/dzah *" . $validatedData['paket_nama'] . "*, ada paket untuk Anda dengan *No. Resi ( " . $kurir . " - " . $validatedData['no_resi'] . " )* pengirim ( *" . $validatedData['nama_pengirim'] . "* ) di Ruang Paket, mohon segera di ambil.

_Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB_

Informasi paket 
" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
            
Jazakumullahu Khoir.
            ";
            if ($request->id_telegram != Null && $request->id_telegram > 0) {

                $cekTele = Http::get('https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $request->id_telegram . '&text=' . $pesan . '&parse_mode=markdown')->status();
                if ($cekTele == 200) {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan');
                } else {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
                }
            }else {
                Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
            }
            $data->save();

            if ($validatedData['lokasi'] == 'Jalancagak') {
                return redirect('/penerimaanPaket');
            } elseif ($validatedData['lokasi'] == 'Wanareja') {
                return redirect('/penerimaanWanareja');
            } else {
                return redirect('/penerimaanSagalaherang');
            }
        } elseif ($request['k_siswa'] && $request['lokasi']) {
            $validatedData = $request->validate([
                'no_resi' => 'required|unique:transaksi_paket',
                'nis' => 'required',
                'ekspedisi_id' => 'required',
                'lokasi' => 'required',
                'k_siswa' => 'required',
                'nama_pengirim' => 'required',
                'paket_nama' => 'required',
                'unit' => 'required',
                'nama_wali' => 'required',
                'no_kamar' => 'required',
                'id_pegawai' => 'required'
            ]);
            $data = new TransaksiPaket();
            $data->no_resi = $validatedData['no_resi'];
            $data->nis = $validatedData['nis'];
            $data->pegawai_id = $validatedData['id_pegawai'];
            $data->ekspedisi_id = $validatedData['ekspedisi_id'];
            $data->lokasi = $validatedData['lokasi'];
            $data->kelompok = $validatedData['k_siswa'];
            $data->nama_penerima = $validatedData['paket_nama'];
            $data->nama_wali = $validatedData['nama_wali'];
            $data->unit = $validatedData['unit'];
            $data->no_kamar = $validatedData['no_kamar'];
            $data->tgl_terima = date('Y-m-d H:i:s');
            $data->nama_pengirim = $validatedData['nama_pengirim'];
            $data->save();
            $pj = Pj::cariPj($validatedData['lokasi']);
            $kurir = TransaksiPaket::kurir($validatedData['ekspedisi_id']);
            $pesan = "*[Informasi Penerimaan Paket]*

Ykh. Ustadz/dzah *" . $validatedData['paket_nama'] . "*, ada paket untuk Murid dengan *No. Resi ( " . $kurir . " - " . $validatedData['no_resi'] . " )* pengirim ( *" . $validatedData['nama_pengirim'] . "* ) di Ruang Paket, mohon segera di ambil.
            
_Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB_
            
Informasi paket 
" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
                        
Jazakumullahu Khoir.";

            if ($request->id_telegram != Null && $request->id_telegram > 0) {

                $cekTele = Http::get('https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $request->id_telegram . '&text=' . $pesan . '&parse_mode=markdown')->status();
                if ($cekTele == 200) {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan');
                } else {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
                }
            }else {
                Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
            }
            if ($validatedData['lokasi'] == 'Jalancagak') {
                return redirect('/penerimaanPaket');
            } elseif ($validatedData['lokasi'] == 'Wanareja') {
                return redirect('/penerimaanWanareja');
            } else {
                return redirect('/penerimaanSagalaherang');
            }
        } else if ($request['k_keluarga'] && $request['lokasi']) {
            $validatedData = $request->validate([
                'no_resi' => 'required|unique:transaksi_paket',
                'pegawai_id' => 'required',
                'ekspedisi_id' => 'required',
                'lokasi' => 'required',
                'k_keluarga' => 'required',
                'nama_pengirim' => 'required',
                'paket_nama' => 'required',
                'unit' => 'required',
                'bidang' => 'required',
                'keterangan' => 'required'
            ]);

            $data = new TransaksiPaket();
            $data->no_resi = $validatedData['no_resi'];
            $data->pegawai_id = $validatedData['pegawai_id'];
            $data->ekspedisi_id = $validatedData['ekspedisi_id'];
            $data->nama_penerima = $validatedData['paket_nama'];
            $data->unit = $validatedData['unit'];
            $data->bidang = $validatedData['bidang'];
            $data->lokasi = $validatedData['lokasi'];
            $data->keterangan = $validatedData['keterangan'];
            $data->kelompok = $validatedData['k_keluarga'];
            $data->tgl_terima = date('Y-m-d H:i:s');
            $data->nama_pengirim = $validatedData['nama_pengirim'];
            $data->save();
            $pj = Pj::cariPj($validatedData['lokasi']);
            $kurir = TransaksiPaket::kurir($validatedData['ekspedisi_id']);

            $pesan = "*[Informasi Penerimaan Paket]*

Ykh. Ustadz/dzah *" . $validatedData['keterangan'] . "*, ada paket untuk Anda dengan *No. Resi ( " . $kurir . " - " . $validatedData['no_resi'] . " )* pengirim ( *" . $validatedData['nama_pengirim'] . "* ) di Ruang Paket, mohon segera di ambil.

_Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB_

Informasi paket 
" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
            
Jazakumullahu Khoir.
            ";
            if ($request->id_telegram != NULL && $request->id_telegram > 0) {
                
                $cekTele = Http::get('https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $request->id_telegram . '&text=' . $pesan . '&parse_mode=markdown')->status();
                if ($cekTele == 200) {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan');
                } else {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
                }
            }else {
                Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
            }
            if ($validatedData['lokasi'] == 'Jalancagak') {
                return redirect('/penerimaanPaket');
            } elseif ($validatedData['lokasi'] == 'Wanareja') {
                return redirect('/penerimaanWanareja');
            } else {
                return redirect('/penerimaanSagalaherang');
            }
        } else {
            $validatedData = $request->validate([
                'no_resi' => 'required|unique:transaksi_paket',
                'ekspedisi_id' => 'required',
                'lokasi' => 'required',
                'k_ltiq' => 'required',
                'nama_pengirim' => 'required',
                'm_ltiq' => 'required',
                'unit' => 'required',
                'nama_penerima' => 'required',
                'id_pegawai' => 'required',
                'nama_wali' => 'required'
            ]);

            $data = new TransaksiPaket();
            $data->nis = $validatedData['m_ltiq'];
            $data->pegawai_id = $validatedData['id_pegawai'];
            $data->no_resi = $validatedData['no_resi'];
            $data->lokasi = $validatedData['lokasi'];
            $data->unit = $validatedData['unit'];
            $data->ekspedisi_id = $validatedData['ekspedisi_id'];
            $data->tgl_terima = date('Y-m-d H:i:s');
            $data->nama_pengirim = $validatedData['nama_pengirim'];
            $data->kelompok = $validatedData['k_ltiq'];
            $data->nama_penerima = $validatedData['nama_penerima'];
            $data->nama_wali = $validatedData['nama_wali'];
            $data->save();
            if ($request->id_telegram && $request->id_telegram != 0) {
                $pj = Pj::cariPj($validatedData['lokasi']);
                $kurir = TransaksiPaket::kurir($validatedData['ekspedisi_id']);
                $pesan = "*[Informasi Penerimaan Paket]*

Ykh. Ustadz/dzah *" . $validatedData['nama_wali'] . "*, ada paket untuk Santri atas nama (*" . $validatedData['nama_penerima'] . "*) dengan *No. Resi ( " . $kurir . " - " . $validatedData['no_resi'] . " )* pengirim ( *" . $validatedData['nama_pengirim'] . "* ) di Ruang Paket, mohon segera di ambil.

_Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB_

Informasi paket 
" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
            
Jazakumullahu Khoir.
            ";
            if ($request->id_telegram != Null && $request->id_telegram > 0) {
                
                $cekTele = Http::get('https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $request->id_telegram . '&text=' . $pesan . '&parse_mode=markdown')->status();
                if ($cekTele == 200) {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan');
                } else {
                    Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
                }
            }else {
                Alert::success('Berhasil', 'No. Resi ' . $validatedData['no_resi'] . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
            }
            }
            if ($validatedData['lokasi'] == 'Jalancagak') {
                return redirect('/penerimaanPaket');
            } elseif ($validatedData['lokasi'] == 'Wanareja') {
                return redirect('/penerimaanWanareja');
            } else {
                return redirect('/penerimaanSagalaherang');
            }
        };
    }

    public function storePengambilan(Request $request)
    {
        $validatedData = $request->validate([
            'nama_pengambil' => 'required',
            'lokasi' => 'required'
        ]);
        $validatedData['status'] = 1;
        $validatedData['user_id'] = Auth::user()->id;
        $validatedData['tgl_ambil'] = date('Y-m-d H:i:s');
        TransaksiPaket::where('id', $request->id)->update($validatedData);


        if ($request->id_telegram != NULL && $request->id_telegram > 0) {
            $pj = Pj::cariPj($validatedData['lokasi']);
            $pesan = "*[Informasi Pengambilan Paket]*

Ykh. Ustadz/dzah *(" . $request->nama_penerima . ")*, paket untuk Anda dengan *No. Resi ( " . $request->j_kurir . " - " . $request->no_uniq . " )* pengirim ( *" . $request->nama_pengirim . "* )sudah diambil oleh *(" . $validatedData['nama_pengambil'] . ")*

Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB

Informasi paket 
" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
            
Jazakumullahu Khoir.
            ";
            if ($request->id_telegram != Null && $request->id_telegram > 0) {
                
                $cekTele = Http::get('https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $request->id_telegram . '&text=' . $pesan . '&parse_mode=markdown')->status();
                if ($cekTele == 200) {
                    Alert::success('Berhasil', 'No. Resi ' . $request->no_uniq . ' Berhasil di tambahkan');
                } else {
                    Alert::success('Berhasil', 'No. Resi ' . $request->no_uniq . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
                }
            }
        }else {
            Alert::success('Berhasil', 'No. Resi ' . $request->no_uniq . ' Berhasil di tambahkan, Telegram Tidak Aktif harap beritahu penerima paket secara manual!');
        }
        if (Auth::user()->lokasi_id == 1) {
            return redirect('/pengambilanCagak');
        } else if (Auth::user()->lokasi_id == 2) {
            return redirect('/pengambilanWanareja');
        } else if (Auth::user()->lokasi_id == 3) {
            return redirect('/pengambilanSagalaherang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * param  \App\Models\TransaksiPaket  $transaksiPaket
     * return \Illuminate\Http\Response
     */

    public function createPengambilanCagak()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        return view('transaksi.pengambilanForm', [
            'title' => 'Form Pengambilan',
            'judulForm' => 'Pengambilan',
            'lokasi' => 'Jalancagak',
            'waktu' => $waktu
        ]);
    }
    public function createPengambilanWanareja()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        return view('transaksi.pengambilanForm', [
            'title' => 'Form Pengambilan',
            'judulForm' => 'Pengambilan',
            'lokasi' => 'Wanareja',
            'waktu' => $waktu
        ]);
    }
    public function createPengambilanSagalaherang()
    {
        $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
        return view('transaksi.pengambilanForm', [
            'title' => 'Form Pengambilan',
            'judulForm' => 'Pengambilan',
            'lokasi' => 'Sagalaherang',
            'waktu' => $waktu
        ]);
    }
    public function edit(TransaksiPaket $transaksiPaket)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * param  \Illuminate\Http\Request  $request
     * param  \App\Models\TransaksiPaket  $transaksiPaket
     * return \Illuminate\Http\Response
     */
    public function update(Request $request, TransaksiPaket $transaksiPaket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * param  \App\Models\TransaksiPaket  $transaksiPaket
     * return \Illuminate\Http\Response
     */
    public function destroy(TransaksiPaket $transaksiPaket)
    {
        //
    }
}
