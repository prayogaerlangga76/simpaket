<?php

namespace App\Http\Controllers;

use App\Models\Lokasi;
use App\Models\Role;
use App\Models\User;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('user.index', [
            'title' => 'User Page',
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lokasi = Lokasi::all();
        $role = Role::all();
        $pegawai = Pegawai::all();
        return view('user.create', [
            'title' => 'Add User Page',
            'role' => $role,
            'pegawai' => $pegawai,
            'lokasi' => $lokasi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role_id' => 'required',
            'pegawai_id' => 'required',
            'lokasi_id' => 'required',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        User::create($validatedData);
        Alert::success('Berhasil', 'User berhasil ditambahkan');
        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        $lokasi = Lokasi::all();
        return view('user.edit', [
            'title' => 'Edit user page',
            'user' => $data,
            'role' => Role::all(),
            'lokasi' => $lokasi,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'email' => 'required',
            'role_id' => 'required',
            'lokasi_id' => 'required',
        ]);

        if($request['password']){
            $validatedData['password'] = Hash::make($request['password']);
        }

        User::where('id', $id)->update($validatedData);
        Alert::success('Berhasil', 'User berhasil diupdate');
        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy('id', $id);
        Alert::success('Berhasil', 'User berhasil dihapus');
        return redirect('/user');
    }
}
