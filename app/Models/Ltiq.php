<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ltiq extends Model
{
    use HasFactory;

    protected $table = 'ltiq';
    protected $connection = 'mysql';
    protected $guarded = ['id'];
    protected $with = ['pegawai', 'pegawai_keluar'];

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'pegawai_id', 'id_pegawai');
    }

    public function pegawai_keluar()
    {
        
        return $this->belongsTo(PegawaiKeluar::class, 'pegawai_id', 'id_pegawai');
    }
}
