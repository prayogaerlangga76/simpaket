<?php

namespace App\Http\Controllers;

use App\Models\Lokasi;
use App\Models\Pj;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pj.index', [
            'title' => 'Penanggung Jawab Page',
            'pj' => Pj::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lokasi = Lokasi::all();
        return view('pj.create', [
            'title' => 'Penanggung Jawab Baru Page',
            'lokasi' => $lokasi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_pegawai' => 'required|unique:pj',
            'no_telepon' => 'required|unique:pj',
            'lokasi_id' => 'required',
        ]);

        Pj::create($validatedData);
        Alert::success('Berhasil', 'Data Berhasil ditambahkan');
        return redirect('/pj');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pj = Pj::findOrFail($id);
        $lokasi = Lokasi::all();
        return view('pj.edit', [
            'title' => 'Edit Penanggung Jawab page',
            'pj' => $pj,
            'lokasi' => $lokasi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_pegawai' => 'required|unique:pj',
            'no_telepon' => 'required|unique:pj',
            'lokasi_id' => 'required',
        ]);

        Pj::where('id', $id)->update($validatedData);
        Alert::success('Berhasil', 'Data Berhasil diubah');
        return redirect('/pj');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pj::destroy('id', $id);
        Alert::success('Berhasil', 'Data Berhasil dihapus');
        return redirect('/pj');
    }
}
