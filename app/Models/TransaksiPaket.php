<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPaket extends Model
{
    use HasFactory;

    protected $table = 'transaksi_paket';

    protected $guarded = ['id'];
    protected $with = ['ekspedisi', 'user', 'pegawai'];

    public function ekspedisi()
    {
        return $this->belongsTo(Ekspedisi::class, 'ekspedisi_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'pegawai_id', 'id_pegawai');
    }

    public function kurir($id)
    {
        $join = TransaksiPaket::join('ekspedisi', 'transaksi_paket.ekspedisi_id', '=', 'ekspedisi.id')
        ->where('transaksi_paket.ekspedisi_id', $id)->first();
        return $join['name'];
    }
    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['kelompok'] ?? false, function ($query, $kelompok) {
            return $query->where('kelompok',  $kelompok);
        });
        $query->when($filters['status'] ?? false, function ($query, $status) {
            if ($status == 1) {
                return $query->where('status', $status);
            } else {
                return $query->where('status', '0');
            }
        });
        $query->when($filters['start_date'] ?? false, function ($query, $start_date) {
            return $query->where('tgl_terima', '>=', $start_date);
        });
        $query->when($filters['end_date'] ?? false, function ($query, $end_date) {
            return $query->where('tgl_terima', '<=', $end_date);
        });

    }
}
