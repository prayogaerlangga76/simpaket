@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Add New ekspedisi<h1>
                                        <form class="user" action="/ekspedisi" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="name"
                                                    name="name" placeholder="Enter Ekspedisi .... ">
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/ekspedisi" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
