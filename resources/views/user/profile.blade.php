@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Update Profile<h1>
                                        <form class="user" action="/profile/{{ Auth::user()->username }}"
                                            method="post">
                                            @method('put')
                                            @csrf
                                            <div class="form-group row">
                                                <h6 for="bidang" class="col-sm-4">Username</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        value="{{ Auth::user()->username }}" name="username"
                                                        id="username" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="email" class="col-sm-4">Email</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        value="{{ Auth::user()->email }}" name="email" id="email" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="password" class="col-sm-4">Password</h6>
                                                <div class="col-md-8">
                                                    <input type="password" class="form-control"
                                                        name="password" id="password">
                                                </div>
                                            </div>
                                            <input type="hidden" class="pegawai_id" name="pegawai_id">
                                            <div class="form-group row">
                                                <h6 for="pegawai_id" class="col-sm-4">Pegawai</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        value="{{ Auth::user()->pegawai->nama_pegawai }}"
                                                        name="nama_pegawai" id="nama_pegawai" readonly>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary mr-2">Save</button>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
