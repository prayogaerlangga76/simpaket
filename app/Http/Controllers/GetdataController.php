<?php

namespace App\Http\Controllers;

use App\Models\Ltiq;
use Illuminate\Http\Request;

class GetdataController extends Controller
{
    public function listLtiq(Request $request)
    {
        $cek = Ltiq::select('id as id', 'nama as text')->where('nama', 'like', "%$request[search]%")->get();
        return response()->json($cek);
    }
}
