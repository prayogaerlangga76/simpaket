<?php

namespace App\Http\Controllers;

use App\Models\Ekspedisi;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class EkspedisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ekspedisi.index', [
            'title' => 'Ekspedisi Page',
            'ekspedisi' => Ekspedisi::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ekspedisi.create', [
            'title' => 'Add new courier page',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:ekspedisi'
        ]);

        $validatedData['slug'] = Str::slug($request['name']);

        Ekspedisi::create($validatedData);

        return redirect('/ekspedisi')->with('success', 'New ekspedisi has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ekspedisi  $ekspedisi
     * @return \Illuminate\Http\Response
     */
    public function show(Ekspedisi $ekspedisi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ekspedisi  $ekspedisi
     * @return \Illuminate\Http\Response
     */
    public function edit(Ekspedisi $ekspedisi)
    {
        $ekspedisi = Ekspedisi::findOrFail($ekspedisi->id);
        return view('ekspedisi.edit', [
            'title' => 'Edit ekspedisi page',
            'ekspedisi' => $ekspedisi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ekspedisi  $ekspedisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ekspedisi $ekspedisi)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:ekspedisi'
        ]);
        $validatedData['slug'] = Str::slug($request['name']);
        Ekspedisi::where('id', $ekspedisi->id)->update($validatedData);
        return redirect('/ekspedisi')->with('success', 'Ekspedisi has been Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ekspedisi  $ekspedisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ekspedisi $ekspedisi)
    {
        Ekspedisi::destroy('id', $ekspedisi->id);
        return redirect('/ekspedisi')->with('success', 'Ekspedisi has been Deleted!');
    }
}
