@extends('layouts.main')

@section('content')
    @php
    use Carbon\Carbon;

    $op = '';
    if (isset($_GET['op'])) {
        $op = $_GET['op'];
    }

    $status = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }
    $lokasi = '';
    if (isset($_GET['lokasi'])) {
        $lokasi = $_GET['lokasi'];
    }
    @endphp
    @if (session()->has('success'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <nav class="navbar">
                <h5 class="font-weight-bold text-primary">Laporan</h5>
                @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
                    <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5"></div>
                    <div class="row ml-5 pl-5">
                        <a href="/laporan/cagak"
                            class="btn btn-primary mr-1 mb-2 d-flex justify-content-center">Jalancagak</a>
                        <a href="/laporan/wanareja"
                            class="btn btn-warning mr-1 d-flex mb-2 justify-content-center">Wanareja</a>
                        <a href="/laporan/sagalaherang"
                            class="btn btn-danger d-flex mb-2 justify-content-center">Sagalaherang</a>
                    </div>
                @endif
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block">
                        <a href="/laporan" class="btn btn-primary">Refresh Data</a>
                    </div>
                </ul>
            </nav>
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="container">
                                <form action="/laporan" method="GET">
                                    <label for="periode">Periode Laporan</label><br>
                                    <div class="input-group mb-3">
                                        <input type="date" class="form-control" name="start_date">
                                        <h4>&nbsp;~&nbsp;</h4>
                                        <input type="date" class="form-control" name="end_date">
                                        <button class="btn btn-primary" type="submit">GET</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 border-left">
                        <label for="periode">Kategori Paket</label><br>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" onclick="show_form('pegawai')" name="k_pegawai"
                                id="inlineRadio6" @if ($op === 'pegawai') {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio6">Pegawai</label>
                        </div>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" onclick="show_form('siswa')" name="k_siswa"
                                id="inlineRadio7" @if ($op === 'siswa') {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio7">Murid</label>
                        </div>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" name="k_keluarga"
                                onclick="show_form('Keluarga Pegawai')" id="inlineRadio3"
                                @if ($op === 'Keluarga Pegawai') {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio3">Keluarga
                                Pegawai</label>
                        </div>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" name="k_ltiq" onclick="show_form('Ltiq')"
                                id="inlineRadio8" @if ($op === 'Ltiq') {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio8">LTIQ</label>
                        </div> <br>
                        <label for="periode">Status Paket</label><br>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" onclick="show_status(1)" id="inlineRadio4"
                                @if ($status == 1) {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio4">Sudah Ambil</label>
                        </div>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input" type="radio" onclick="show_status('0')" name="status"
                                id="inlineRadio5" @if ($status == '0') {{ 'checked' }} @endif>
                            <label class="form-check-label btn btn-outline-secondary" for="inlineRadio5">Belum Ambil</label>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered example" id="dataTable2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No. Resi</th>
                            <th>Tanggal Terima</th>
                            <th>Pengirim</th>
                            <th>Penerima</th>
                            <th>Unit/Kamar</th>
                            <th>Pengambil</th>
                            <th>Nama Keluarga</th>
                            <th>Petugas</th>
                            @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                <th>Aksi</th>
                            @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="9">Total Paket</th>
                            <th>{{ $count }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($laporan as $l)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $l->no_resi }}</td>
                                <td>{{ Carbon::parse($l->tgl_terima)->translatedFormat('l, d F Y') }}</td>
                                <td>{{ $l->nama_pengirim }}</td>
                                <td>{{ $l->nama_penerima }}</td>
                                <td>{{ $l->unit }}</td>
                                <td>{{ $l->nama_pengambil }}</td>
                                <td>{{ $l->keterangan }}</td>
                                @if ($l->user_id != null)
                                    <td>{{ $l->user->pegawai->nama_pegawai }}</td>
                                @else
                                    <td></td>
                                @endif
                                @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                    <td>
                                        <form action="/laporan/{{ $l->id }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8"> No records found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function show_form(a) {
            window.location.href = "?op=" + a;
        }

        function show_status(b) {
            window.location.href = "?status=" + b;
        }
    </script>
    <script>
        function show_lokasi(c) {
            window.location.href = "?lokasi=" + c;
        }
    </script>
@endsection
