<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index', [
            'title' => 'SIMPAKET LOGIN'
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if($credentials['username'] == 'prayogaep'){
            if (Auth::attempt($credentials)){
                $request->session()->regenerate();
                Alert::success('Berhasil', 'Selamat Datang ' . Auth::user()->pegawai->nama_pegawai);
                return redirect()->intended('/');
            }
            return back()->with('loginError', 'User tidak cocok dengan database kami!');
        }


        
        $apiURL = 'http://192.168.223.4/api/login_pegawai_siswa.php';
        $postInput = [
            'token' => 'b86fae77a0a7ff98f2d78405a9e851a1',
             'username' => $credentials['username'],
             'password' => $credentials['password']
        ];
                
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $apiURL, ['form_params' => $postInput]);
        
        $statusCode = $response->getStatusCode();
        $responseBody = json_decode($response->getBody(), true);
        if($responseBody['data_user'] != null) {
            
            $user_data = User::where('username', $responseBody['data_user']['email'])->first();
            // dd($user_data);
            if ($user_data != null)
            {
                if (Auth::attempt($credentials)){
                    $request->session()->regenerate();
                    Alert::success('Berhasil', 'Selamat Datang ' . Auth::user()->pegawai->nama_pegawai);
                    return redirect()->intended('/');
                }
            } else {
                $data = new User();
                $data->username = $responseBody['data_user']['email'];
                $data->pegawai_id = $responseBody['data_user']['id_pegawai'];
                $data->role_id = 4;
                $data->password = Hash::make($credentials['password']);
                $data->email = $responseBody['data_user']['email'];
    
                if ($responseBody['data_user']['lokasi_kerja'] == 'JALANCAGAK')
                {
                    $data->lokasi_id = 1;
                }elseif ($responseBody['data_user']['lokasi_kerja'] == 'WANAREJA')
                {
                    $data->lokasi_id = 2;
                } else {
                    $data->lokasi_id = 3;
                }
                $data->save();
                if(Auth::attempt($credentials)){
                    $request->session()->regenerate();
                    Alert::success('Berhasil', 'Selamat Datang ');
                    return redirect()->intended('/');
                }
            }
        }
        return back()->with('loginError', 'User tidak cocok dengan database kami!');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
