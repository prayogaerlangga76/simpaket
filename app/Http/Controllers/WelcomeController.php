<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\TransaksiPaket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    public function index()
    {
        if (Auth::user()->role_id == 1) {
            $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
            $dataPerDay = TransaksiPaket::whereBetween(DB::raw('date(tgl_terima)'), [date('Y-m-d'), date('Y-m-d')])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
            $belumAmbil = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
            $paketTersedia = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'ASC')->get();
            $paketPegawai = TransaksiPaket::where('kelompok', 'pegawai')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
            $paketSiswa = TransaksiPaket::where('kelompok', 'siswa')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
            $paketLtiq = TransaksiPaket::where('kelompok', 'ltiq')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
            $paketKeluargaPegawai = TransaksiPaket::where('kelompok', 'Keluarga Pegawai')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
        } else {
            $waktu = Carbon::parse(time())->translatedFormat('l, d F Y');
            if (request()->op) {
                $belumAmbil = TransaksiPaket::where('status', 0)->where('lokasi', request()->op)->count();
                $dataPerDay = TransaksiPaket::whereBetween(DB::raw('date(tgl_terima)'), [date('Y-m-d'), date('Y-m-d')])->where('lokasi', request()->op)->get()->count();
                $paketPegawai = TransaksiPaket::where('kelompok', 'pegawai')->where('status', 0)->where('lokasi', request()->op)->count();
                $paketSiswa = TransaksiPaket::where('kelompok', 'siswa')->where('status', 0)->where('lokasi', request()->op)->count();
                $paketLtiq = TransaksiPaket::where('kelompok', 'ltiq')->where('status', 0)->where('lokasi', request()->op)->count();
                $paketKeluargaPegawai = TransaksiPaket::where('kelompok', 'Keluarga Pegawai')->where('status', 0)->where('lokasi', request()->op)->count();
                $yearly = TransaksiPaket::select(DB::raw("COUNT(*) as count"))
                    ->whereYear('created_at', date('Y'))->where('lokasi', request()->op)
                    ->groupBy(DB::raw("Month(created_at)"))
                    ->pluck('count');
                $months = TransaksiPaket::select(DB::raw("MONTHNAME(created_at) as month "))
                    ->whereYear('created_at', date('Y'))->where('lokasi', request()->op)
                    ->groupBy(DB::raw("MONTHNAME(created_at)"))
                    ->pluck('month');
                $datas = [];
                foreach ($months as $index => $month) {
                    $datas[] = [
                        "name" => "'$month'",
                        "y" => $yearly[$index]
                    ];
                }
                $paketTersedia = TransaksiPaket::where('status', 0)->where('lokasi', request()->op)->orderBy('tgl_terima', 'ASC')->paginate(10)->withQueryString();
            } else {
                $dataPerDay = TransaksiPaket::whereBetween(DB::raw('date(tgl_terima)'), [date('Y-m-d'), date('Y-m-d')])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
                $belumAmbil = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
                $paketTersedia = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'ASC')->get();
                $paketPegawai = TransaksiPaket::where('kelompok', 'pegawai')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
                $paketSiswa = TransaksiPaket::where('kelompok', 'siswa')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
                $paketLtiq = TransaksiPaket::where('kelompok', 'ltiq')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
                $paketKeluargaPegawai = TransaksiPaket::where('kelompok', 'Keluarga Pegawai')->where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->count();
                $yearly = TransaksiPaket::select(DB::raw("COUNT(*) as count"))
                    ->whereYear('created_at', date('Y'))->where('lokasi', Auth::user()->lokasi->nama_lokasi)
                    ->groupBy(DB::raw("Month(created_at)"))
                    ->pluck('count');
                $months = TransaksiPaket::select(DB::raw("MONTHNAME(created_at) as month "))
                    ->whereYear('created_at', date('Y'))->where('lokasi', Auth::user()->lokasi->nama_lokasi)
                    ->groupBy(DB::raw("MONTHNAME(created_at)"))
                    ->pluck('month');
                $datas = [];
                foreach ($months as $index => $month) {
                    $datas[] = [
                        "name" => "'$month'",
                        "y" => $yearly[$index]
                    ];
                }
            }
        }

        if (Auth::user()->role_id == 1) {
            return view('welcome', [
                'title' => 'SIMPAKET | Dashboard',
                'waktu' => $waktu,
                'dataPerDay' => $dataPerDay,
                'belumAmbil' => $belumAmbil,
                'paketPegawai' => $paketPegawai,
                'paketSiswa' => $paketSiswa,
                'paketLtiq' => $paketLtiq,
                'paketTersedia' => $paketTersedia,
                'paketKPegawai' => $paketKeluargaPegawai,
            ]);
        } else {
            return view('welcome', [
                'title' => 'SIMPAKET | Dashboard',
                'waktu' => $waktu,
                'dataPerDay' => $dataPerDay,
                'belumAmbil' => $belumAmbil,
                'paketPegawai' => $paketPegawai,
                'paketSiswa' => $paketSiswa,
                'paketLtiq' => $paketLtiq,
                'paketTersedia' => $paketTersedia,
                'paketKPegawai' => $paketKeluargaPegawai,
                'yearly' => $datas,
            ]);
        }
    }
}
