<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PegawaiKeluar extends Model
{
    use HasFactory;
    protected $table = 'pegawai_master_keluar_v';
    protected $connection = 'mysql2';
}
