<?php

namespace App\Console\Commands;

use App\Models\Pj;
use GuzzleHttp\Client;
use App\Models\TransaksiPaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Reminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:paket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = TransaksiPaket::select('*')->where('status', 0)->get();
        foreach ($data as $d) {
            $pj = Pj::cariPj($d->lokasi);
            $kurir = TransaksiPaket::kurir($d->ekspedisi_id);
            $pesan = "*[Informasi Penerimaan Paket]*
    
Ykh. Ustadz/dzah *" . $d->nama_penerima . "*, ada paket untuk Anda dengan *No. Resi ( " . $kurir . " - " . $d->no_resi . " )* pengirim ( *" . $d->nama_pengirim . "* ) di Ruang Paket, mohon segera di ambil.
    
_Pengambilan paket hanya dilakukan pada jam kerja.
Hari : Senin s/d Sabtu
Jam : 08.00 s/d 16.00 WIB_
    
Informasi paket 
@" . $pj[0] . " " . $pj[1] . " (" . $pj[2] . ")
    
Jazakumullahu Khoir.
                    ";
            $pesan = urlencode($pesan);
            $url = 'https://api.telegram.org/bot189811622:AAHXb0WKCeze3KmVa6oMrp4gd1ZRXCQjBiQ/sendMessage?chat_id=' . $d->pegawai->id_telegram . '&text=' . $pesan . '&parse_mode=markdown';
            $client = new Client();
            $client->post($url);
        }
    }
}
