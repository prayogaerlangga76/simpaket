<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPaketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_paket', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pegawai_id')->nullable();
            $table->foreignId('ekspedisi_id');
            $table->foreignId('nis')->nullable();
            $table->string('no_resi')->unique();
            $table->string('nama_pengirim');
            $table->string('lokasi');
            $table->string('keterangan')->nullable();
            $table->string('kelompok');
            $table->string('nama_penerima');
            $table->string('unit');
            $table->string('bidang')->nullable();
            $table->string('no_kamar')->nullable();
            $table->string('nama_wali')->nullable();
            $table->integer('status')->default(0);
            $table->string('nama_pengambil')->nullable();
            $table->dateTime('tgl_terima', $precision = 0);
            $table->dateTime('tgl_ambil', $precision = 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_paket');
    }
}
