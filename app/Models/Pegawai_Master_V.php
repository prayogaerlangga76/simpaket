<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai_Master_V extends Model
{
    use HasFactory;
    protected $table = 'pegawai_master_v';
    protected $connection = 'mysql2';
}
