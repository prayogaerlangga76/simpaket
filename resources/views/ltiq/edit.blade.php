@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Tambah data Santri LTIQ<h1>
                                        <form class="user" action="/ltiq/{{ $data->id }}" method="post">
                                            @method('put')
                                            @csrf
                                            <div class="form-group row">
                                                <h5 for="m_ltiq" class="col-sm-4">Nama Santri</h5>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="nama" name="nama"
                                                        value="{{ $data->nama }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h5 for="unit" class="col-sm-4">Unit</h5>
                                                <div class="col-md-8">
                                                    <select class="custom-select" name="unit">
                                                        <option selected disabled>Pilih Asrama LTIQ Putra / LTIQ Putri
                                                        </option>
                                                        @if ($data->unit == 'LTIQ Putra')
                                                            <option value="LTIQ Putra" selected>LTIQ Putra</option>
                                                            <option value="LTIQ Putri">LTIQ Putri</option>
                                                        @endif
                                                        @if ($data->unit == 'LTIQ Putri')
                                                            <option value="LTIQ Putri">LTIQ Putri</option>
                                                            <option value="LTIQ Putri" selected>LTIQ Putri</option>
                                                        @endif
                                                        @if ($data->unit != 'LTIQ Putra' && $data->unit != 'LTIQ Putri')
                                                            <option value="LTIQ Putri">LTIQ Putri</option>
                                                            <option value="LTIQ Putri">LTIQ Putri</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h5 for="m_ltiq" class="col-sm-4">Nama Musyrif</h5>
                                                <div class="col-md-8">
                                                    @if ($data->pegawai != NULL)
                                                        
                                                    <input type="text" class="form-control"
                                                        value="{{ $data->pegawai->nama_pegawai }}" readonly>
                                                        @else
                                                        <input type="text" class="form-control"
                                                            value="Pegawai tidak terdaftar" readonly>
                                                        
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h5 for="pegawai_id" class="col-sm-4 ">Ganti Musyrif</h5>
                                                <div class="col-md-8">
                                                    <select
                                                        class="form-control js-example-data-array @error('pegawai_id') is-invalid @enderror"
                                                        id="nama_musyrif" name="pegawai_id"></select>
                                                    @error('pegawai_id')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/ltiq" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
