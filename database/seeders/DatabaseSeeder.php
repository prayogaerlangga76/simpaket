<?php

namespace Database\Seeders;

use App\Models\Pj;
use App\Models\Role;
use App\Models\User;
use App\Models\Lokasi;
use App\Models\Ekspedisi;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory()->create();

        Role::create([
            'name' => 'Staff',
            'slug' => 'staff',
        ]);
        Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
        ]);

        Role::create([
            'name' => 'IT',
            'slug' => 'it',
        ]);
        Role::create([
            'name' => 'Default',
            'slug' => 'default',
        ]);

        Lokasi::create([
            'nama_lokasi' => 'Jalancagak'
        ]);
        Lokasi::create([
            'nama_lokasi' => 'Wanareja'
        ]);
        Lokasi::create([
            'nama_lokasi' => 'Sagalaherang'
        ]);

        Ekspedisi::create([
            'name' => 'JNE',
            'slug' => 'jne',
        ]);
        Ekspedisi::create([
            'name' => 'J&T Express',
            'slug' => 'j&t-express',
        ]);
        Ekspedisi::create([
            'name' => 'Anteraja',
            'slug' => 'anteraja',
        ]);
        Ekspedisi::create([
            'name' => 'Sicepat Express',
            'slug' => 'sicepat-express',
        ]);
        Ekspedisi::create([
            'name' => 'Ninja Express',
            'slug' => 'ninja-express',
        ]);

        Pj::create([
            'nama_pegawai' => 'Edi Setiadi',
            'no_telepon' => '0823 1568 8136',
            'lokasi_id' => 1
        ]);
        Pj::create([
            'nama_pegawai' => 'Eka Rahman',
            'no_telepon' => '0853 5112 3549',
            'lokasi_id' => 2
        ]);

        User::create([
            'username' => 'prayogaep',
            'email' => 'prayoga@gmail.com',
            'email_verified_at' => now(),
            'role_id' => 3,
            'pegawai_id' => 3876,
            'lokasi_id' => 1,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
    }
}
