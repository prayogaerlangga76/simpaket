<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ $title }}</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" integrity="sha512-kq3FES+RuuGoBW3a9R2ELYKRywUEQv0wvPTItv3DSGqjpbNtGWVdvT8qwdKkqvPzT93jp8tSF4+oN4IeTEIlQA==" crossorigin="anonymous" referrerpolicy="no-referrer" />    

</head>

<body id="page-top">

    
    @yield('content')

    <!-- Bootstrap core JavaScript-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
    <!-- js untuk select2  -->
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'pdf', 'print'
                ]
            });
            table.buttons().container()
                .appendTo('.example .col-md-6:eq(0)');
        });
    </script>

    <script>
        $(document).on('change', '#nama_penerima_pegawai', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPegawai/" + $('#nama_penerima_pegawai').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('#bidang').val(response.bidang);
                    $('#unit').val(response.unit);
                    $('#paket_nama').val(response.nama_pegawai);
                    $('#id_telegram').val(response.id_telegram);
                }

            });
        });
        $(document).on('change', '#pegawai_id', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPegawai/" + $('#pegawai_id').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('.pegawai_id').val(response.id_pegawai);
                }

            });
        });
        $(document).on('change', '#nama_penerima_murid', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showMurid/" + $('#nama_penerima_murid').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('#nama_wali').val(response.nama_pegawai);
                    $('#id_telegram').val(response.id_telegram);
                    $('#paket_nama').val(response.nama);
                    $('#no_kamar').val(response.nama_kamar);
                    $('#unit').val(response.nama_asrama);
                }

            });
        });
        $(document).on('change', '#js-transaksi-data', function() {
            $.ajax({
                type: "GET",
                url: "/penerimaan/showPaket/" + $('.js-transaksi-data').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('#kelompok').val(response.kelompok);
                    $('#nama_penerima').val(response.nama_penerima);
                    $('#bidang').val(response.bidang);
                    $('#unit').val(response.unit);
                    $('#no_kamar').val(response.no_kamar);
                    $('#keterangan').val(response.keterangan);
                    $('#nama_pengirim').val(response.nama_pengirim);
                    $('#id').val(response.id);
                }

            });
        });
    </script>
    <script type=text/javascript>
        $(function() {
            $('#nama_penerima_pegawai').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan Nama Penerima',
                ajax: {
                    dataType: 'json',
                    url: 'http://simpaket.test/penerimaan/listPegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
        $(function() {
            $('#pegawai_id').select2({
                minimumInputLength: 3,
                theme: "bootstrap",
                allowClear: true,
                ajax: {
                    dataType: 'json',
                    url: 'http://simpaket.test/penerimaan/listPegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
        $(function() {
            $('.js-transaksi-data').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan No Resi',
                ajax: {
                    dataType: 'json',
                    url: 'http://simpaket.test/penerimaan/listPaket',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-transaksi-data option:selected").text();
            });
        });
        $(function() {
            $('#nama_penerima_murid').select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukan Nama Penerima',
                ajax: {
                    dataType: 'json',
                    url: 'http://simpaket.test/penerimaan/listMurid',
                    delay: 800,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            }).on('select2:select', function(evt) {
                var data = $(".js-example-basic-single option:selected").text();
            });
        });
    </script>
    @stack('script')
</body>

</html>
