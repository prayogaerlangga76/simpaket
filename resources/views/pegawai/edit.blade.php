@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Add New pegawai<h1>
                                        <form class="user" action="/pegawai/{{ $pegawai->id }}" method="post">
                                            @method('put')
                                            @csrf
                                            <div class="form-group">
                                                <input type="hidden" class="form-control form-control-user" id="user_id"
                                                    name="user_id" value="{{ $pegawai->user_id }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="nama_pegawai"
                                                    name="nama_pegawai" value="{{ $pegawai->nama_pegawai }}" placeholder="Enter pegawai .... ">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="bidang"
                                                    name="bidang" value="{{ $pegawai->bidang }}" placeholder="Enter bidang .... ">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="unit"
                                                    name="unit" placeholder="Enter unit .... " value="{{ $pegawai->unit }}">
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/pegawai" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
