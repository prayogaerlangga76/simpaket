@extends('layouts.main')

@section('content')
    @php
    use Carbon\Carbon;
    @endphp
    @if (session()->has('success'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <nav class="navbar">
                <h5 class="font-weight-bold text-primary">Laporan</h5>
                @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
                    {{-- <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5"></div>
                    <div class="row ml-5 pl-5">
                        <a href="/laporan/cagak"
                            class="btn btn-primary mr-1 mb-2 d-flex justify-content-center">Jalancagak</a>
                        <a href="/laporan/wanareja"
                            class="btn btn-warning mr-1 d-flex mb-2 justify-content-center">Wanareja</a>
                        <a href="/laporan/sagalaherang"
                            class="btn btn-danger d-flex mb-2 justify-content-center">Sagalaherang</a>
                    </div> --}}
                    <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5 pr-5"></div>
                    <div class="row mr-5"></div>
                    <div class="row ml-5 pl-4">
                        <a href="/laporan/cagak"
                            class="btn btn-primary mr-1 mb-2 d-flex justify-content-center">Jalancagak</a>
                        <a href="/laporan/wanareja"
                            class="btn btn-warning mr-1 d-flex mb-2 justify-content-center">Wanareja</a>
                        <a href="/laporan/sagalaherang"
                            class="btn btn-danger mr-1 d-flex mb-2 justify-content-center">Sagalaherang</a>
                    </div>

                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block">
                            <a href="/laporan" class="btn btn-primary  d-flex mb-2 justify-content-center">Refresh
                                Data</a>
                        </div>
                    </ul>
                @else
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block">
                            <a href="/laporan" class="btn btn-primary  d-flex mb-2 justify-content-center">Refresh
                                Data</a>
                        </div>
                    </ul>
                @endif
            </nav>
        </div>
        <div class="card-body">
            <p class="text-right mr-4">Lokasi : {{ $lokasi }} </p>
            <div class="row">
                <div class="col-md">
                    @if (Request::is('laporan'))
                        <form action="/laporan" method="GET">
                    @endif
                    @if (Request::is('laporan/cagak'))
                        <form action="/laporan/cagak" method="GET">
                    @endif
                    @if (Request::is('laporan/wanareja'))
                        <form action="/laporan/wanareja" method="GET">
                    @endif
                    @if (Request::is('laporan/sagalaherang'))
                        <form action="/laporan/sagalaherang" method="GET">
                    @endif
                    <div class="form-group row">
                        <div class="container">
                            <label for="periode">Periode Laporan</label><br>
                            <div class="input-group mb-3">
                                <input type="date" class="form-control" name="start_date">
                                <h4>&nbsp;~&nbsp;</h4>
                                <input type="date" class="form-control" name="end_date">
                            </div>
                            <label for="periode">Status Paket</label><br>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input" type="radio" value="1" name="status" id="inlineRadio4"
                                    {{ request()->status == 1 ? 'checked' : '' }}>

                                <label class="form-check-label btn btn-outline-secondary" for="inlineRadio4">Sudah
                                    Ambil</label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input" type="radio" value="2" name="status" id="inlineRadio5"
                                    {{ request()->status == 2 ? 'checked' : '' }}>
                                <label class="form-check-label btn btn-outline-secondary" for="inlineRadio5">Belum
                                    Ambil</label>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="periode">Kategori Paket</label><br>
                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" value="pegawai" name="kelompok" id="inlineRadio6"
                            {{ request()->kelompok == 'pegawai' ? 'checked' : '' }}>
                        <label class="form-check-label btn btn-outline-secondary" for="inlineRadio6">Pegawai</label>
                    </div>
                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" value="siswa" name="kelompok" id="inlineRadio7"
                            {{ request()->kelompok == 'siswa' ? 'checked' : '' }}>
                        <label class="form-check-label btn btn-outline-secondary" for="inlineRadio7">Murid</label>
                    </div>
                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" value="Keluarga Pegawai" name="kelompok"
                            id="inlineRadio3" {{ request()->kelompok == 'Keluarga Pegawai' ? 'checked' : '' }}>
                        <label class="form-check-label btn btn-outline-secondary" for="inlineRadio3">Keluarga
                            Pegawai</label>
                    </div>
                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" name="kelompok" value="Ltiq" id="inlineRadio8"
                            {{ request()->kelompok == 'Ltiq' ? 'checked' : '' }}>
                        <label class="form-check-label btn btn-outline-secondary" for="inlineRadio8">LTIQ</label>
                    </div> <br>
                    <div class="d-flex justify-content-center bg-primary card mt-4">
                        <button type="submit" class="btn btn-primary">Cari Data</button>
                    </div>
                    </form>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered example" id="dataTable2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No. Resi</th>
                            <th>Tanggal Terima</th>
                            <th>Pengirim</th>
                            <th>Penerima</th>
                            <th>Nama Keluarga</th>
                            <th>Unit/Kamar</th>
                            <th>Pengambil</th>
                            <th>Petugas</th>
                            @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                <th>Aksi</th>
                            @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                <th colspan="9">Total Paket</th>
                            @else
                                <th colspan="
                                                                                                                    8">
                                    Total
                                    Paket
                                </th>
                            @endif
                            <th>{{ $count }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($laporan as $l)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $l->no_resi }}</td>
                                <td>{{ Carbon::parse($l->tgl_terima)->translatedFormat('l, d F Y') }}</td>
                                <td>{{ $l->nama_pengirim }}</td>
                                <td>{{ $l->nama_penerima }}</td>
                                <td>{{ $l->keterangan }}</td>
                                <td>{{ $l->unit }}</td>
                                <td>{{ $l->nama_pengambil }}</td>
                                @if ($l->user_id != null)
                                    <td>{{ $l->user->pegawai->nama_pegawai }}</td>
                                @else
                                    <td></td>
                                @endif
                                @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2)
                                    <td>
                                        <form action="/laporan/{{ $l->id }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8"> No records found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function show_form(a) {
            window.location.href = "?op=" + a;
        }

        function show_status(b) {
            window.location.href = "?status=" + b;
        }
    </script>
    <script>
        function show_lokasi(c) {
            window.location.href = "?lokasi=" + c;
        }
    </script>
@endsection
