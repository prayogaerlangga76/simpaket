<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\TransaksiPaket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class LaporanController extends Controller
{
    public function index()
    {
        if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2) {
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $kelompok = request()->kelompok;
            //     $status = request()->status;
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date ? request()->start_date : '', $end_date ? request()->end_date : ''])->where('kelompok', request()->kelompok)->where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date ? request()->start_date : '', $end_date ? request()->end_date : ''])->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else { 
            //     $data = TransaksiPaket::orderBy('tgl_terima', 'DESC')->get();
            //     $count = TransaksiPaket::orderBy('tgl_terima', 'DESC')->get()->count();
            // }
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     }
            // } else if (request()->lokasi) {
            //     $data = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->get();
            //     $count = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->get()->count();
            // } else {
            // }

        } else {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     }
            // } else {
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        }
        return view('laporan.index', [
            'title' => 'Laporan Paket',
            'lokasi' => 'Seluruh Lokasi',
            'laporan' => $data,
            'count' => $count
        ]);
    }

    public function laporanCagak()
    {
        if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2) {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //         $count = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            //     }
            // } else if (request()->lokasi) {
            //     $data = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //     $count = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            // } else {
            //     $data = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Jalancagak')->get();
            //     $count = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Jalancagak')->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', 'Jalancagak')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', 'Jalancagak')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        } else {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     }
            // } else {
            //     $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get();
            //     $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        }
        return view('laporan.index', [
            'title' => 'Laporan Paket',
            'lokasi' => 'Jalancagak',
            'laporan' => $data,
            'count' => $count
        ]);
    }
    public function laporanWanareja()
    {
        if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2) {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get();
            //         $count = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            //     }
            // } else if (request()->lokasi) {
            //     $data = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Wanareja')->get();
            //     $count = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            // } else {
            //     $data = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Wanareja')->get();
            //     $count = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Wanareja')->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', 'Wanareja')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', 'Wanareja')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        } else {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     }
            // } else {
            //     $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get();
            //     $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        }
        return view('laporan.index', [
            'title' => 'Laporan Paket',
            'lokasi' => 'Wanareja',
            'laporan' => $data,
            'count' => $count
        ]);
    }
    public function laporanSagalaherang()
    {
        if (Auth::user()->role_id == 3 || Auth::user()->role_id == 2) {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //         $count = TransaksiPaket::where('status', 0)->orderBy('tgl_ambil', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            //     }
            // } else if (request()->lokasi) {
            //     $data = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //     $count = TransaksiPaket::where('lokasi', request()->lokasi)->orderBy('tgl_terima', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            // } else {
            //     $data = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Sagalaherang')->get();
            //     $count = TransaksiPaket::orderBy('tgl_terima', 'DESC')->where('lokasi', 'Sagalaherang')->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', 'Sagalaherang')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', 'Sagalaherang')->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        } else {
            // if (request()->start_date || request()->end_date) {
            //     $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            //     $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //     $data = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::whereBetween('tgl_terima', [$start_date, $end_date])->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->op) {
            //     $data = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //     $count = TransaksiPaket::where('kelompok', request()->op)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            // } else if (request()->status == 1 || request()->status == '0') {
            //     if (request()->status == 1) {
            //         $data = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', request()->status)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     } else {
            //         $data = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get();
            //         $count = TransaksiPaket::where('status', 0)->where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_ambil', 'DESC')->get()->count();
            //     }
            // } else {
            //     $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get();
            //     $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->get()->count();
            // }
            if (request('kelompok') == TRUE) {
                $kelompok = TransaksiPaket::firstWhere('kelompok', request('kelompok'));
            }
            if (request('status') == TRUE) {
                $status = TransaksiPaket::firstWhere('status', request('status'));
            }
            if (request('start_date') == TRUE) {
                $tanggal_awal = TransaksiPaket::firstWhere('tgl_terima', request('start_date'));
            }
            if (request('end_date') == TRUE) {
                $tanggal_akhir = TransaksiPaket::firstWhere('tgl_terima', request('end_date'));
            }
            $data = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get();
            $count = TransaksiPaket::where('lokasi', Auth::user()->lokasi->nama_lokasi)->orderBy('tgl_terima', 'DESC')->filter(request(['kelompok', 'status', 'start_date', 'end_date']))->get()->count();
        }
        return view('laporan.index', [
            'title' => 'Laporan Paket',
            'lokasi' => 'Sagalaherang',
            'laporan' => $data,
            'count' => $count
        ]);
    }

    public function laporanGrafik()
    {
        if (request()->periode) {
            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->select(DB::raw('count(transaksi_paket.no_resi) as total'), 'ekspedisi.name')->groupBy('ekspedisi_id')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->groupBy('lokasi')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'lokasi');

            if ($paketPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'kelompok');
            if ($paketPerKategori->count() == 0) {
                $dataKategori = [];
            }
        } else {

            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->selectRaw('count(transaksi_paket.no_resi) as total, ekspedisi.name')->groupBy('ekspedisi_id')->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->groupBy('lokasi')->pluck('total', 'lokasi');
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->pluck('total', 'kelompok');
            if ($kurirPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            if ($kurirPerWilayah->count() == 0) {
                $dataKategori = [];
            }
        }

        foreach ($kurirPerWilayah as $index => $kurir) {
            $datakurir[] = [
                "name" => "'$index'",
                "y" => $kurir
            ];
        }
        foreach ($paketPerKategori as $index => $kategori) {
            $dataKategori[] = [
                "name" => "'$index'",
                "y" => $kategori
            ];
        }
        foreach ($paketPerWilayah as $index => $paket) {
            $dataPaket[] = [
                "name" => "'$index'",
                "y" => $paket
            ];
        }

        return view('laporan.grafik', [
            'title' => 'Laporan Grafik',
            'lokasi' => 'Seluruh Lokasi',
            'kurir' => $datakurir,
            'paket' => $dataPaket,
            'kategori' => $dataKategori
        ]);
    }

    public function grafikCagak()
    {
        if (request()->periode) {
            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->select(DB::raw('count(transaksi_paket.no_resi) as total'), 'ekspedisi.name')->groupBy('ekspedisi_id')->where('lokasi', 'Jalancagak')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->groupBy('lokasi')->where('lokasi', 'Jalancagak')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'lokasi');

            if ($paketPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->whereMonth('tgl_terima', '=', request()->periode)->where('lokasi', 'Jalancagak')->pluck('total', 'kelompok');
            if ($paketPerKategori->count() == 0) {
                $dataKategori = [];
            }
        } else {

            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->selectRaw('count(transaksi_paket.no_resi) as total, ekspedisi.name')->where('lokasi', 'Jalancagak')->groupBy('ekspedisi_id')->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->where('lokasi', 'Jalancagak')->groupBy('lokasi')->pluck('total', 'lokasi');
            if ($kurirPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->where('lokasi', 'Jalancagak')->groupBy('kelompok')->pluck('total', 'kelompok');
            if ($kurirPerWilayah->count() == 0) {
                $dataKategori = [];
            }
        }

        foreach ($kurirPerWilayah as $index => $kurir) {
            $datakurir[] = [
                "name" => "'$index'",
                "y" => $kurir
            ];
        }
        foreach ($paketPerKategori as $index => $kategori) {
            $dataKategori[] = [
                "name" => "'$index'",
                "y" => $kategori
            ];
        }
        foreach ($paketPerWilayah as $index => $paket) {
            $dataPaket[] = [
                "name" => "'$index'",
                "y" => $paket
            ];
        }

        return view('laporan.grafik', [
            'title' => 'Laporan Grafik',
            'lokasi' => 'Jalancagak',
            'kurir' => $datakurir,
            'paket' => $dataPaket,
            'kategori' => $dataKategori
        ]);
    }
    public function grafikWanareja()
    {
        if (request()->periode) {
            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->select(DB::raw('count(transaksi_paket.no_resi) as total'), 'ekspedisi.name')->groupBy('ekspedisi_id')->where('lokasi', 'Wanareja')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'name');
            // dd($kurirPerWilayah->count());
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->groupBy('lokasi')->where('lokasi', 'Wanareja')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'lokasi');

            if ($paketPerWilayah->count() == 0) {
                $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->where('lokasi', 'Wanareja')->groupBy('lokasi')->pluck('total', 'lokasi');
                if ($paketPerWilayah->count() == 0) {
                    $dataPaket = [];
                }
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->whereMonth('tgl_terima', '=', request()->periode)->where('lokasi', 'Wanareja')->pluck('total', 'kelompok');
            if ($paketPerKategori->count() == 0) {
                $dataKategori = [];
            }
        } else {

            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->selectRaw('count(transaksi_paket.no_resi) as total, ekspedisi.name')->where('lokasi', 'Wanareja')->groupBy('ekspedisi_id')->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->where('lokasi', 'Wanareja')->groupBy('lokasi')->pluck('total', 'lokasi');
            if ($kurirPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->where('lokasi', 'Wanareja')->groupBy('kelompok')->pluck('total', 'kelompok');
            if ($kurirPerWilayah->count() == 0) {
                $dataKategori = [];
            }
        }

        foreach ($kurirPerWilayah as $index => $kurir) {
            $datakurir[] = [
                "name" => "'$index'",
                "y" => $kurir
            ];
        }
        foreach ($paketPerKategori as $index => $kategori) {
            $dataKategori[] = [
                "name" => "'$index'",
                "y" => $kategori
            ];
        }
        foreach ($paketPerWilayah as $index => $paket) {
            $dataPaket[] = [
                "name" => "'$index'",
                "y" => $paket
            ];
        }

        return view('laporan.grafik', [
            'title' => 'Laporan Grafik',
            'lokasi' => 'Wanareja',
            'kurir' => $datakurir,
            'paket' => $dataPaket,
            'kategori' => $dataKategori
        ]);
    }
    public function grafikSagalaherang()
    {
        if (request()->periode) {
            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->select(DB::raw('count(transaksi_paket.no_resi) as total'), 'ekspedisi.name')->groupBy('ekspedisi_id')->where('lokasi', 'Sagalaherang')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->selectRaw('count(transaksi_paket.no_resi) as total, ekspedisi.name')->groupBy('ekspedisi_id')->where('lokasi', 'Sagalaherang')->pluck('total', 'name');
                if ($kurirPerWilayah->count() == 0) {
                    $datakurir = [];
                    Alert::error('Oops', 'Data Kurir tidak ditemukan');
                }
                Alert::error('Oops', 'Data Kurir tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->groupBy('lokasi')->where('lokasi', 'Sagalaherang')->whereMonth('tgl_terima', '=', request()->periode)->pluck('total', 'lokasi');

            if ($paketPerWilayah->count() == 0) {
                $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->where('lokasi', 'Sagalaherang')->groupBy('lokasi')->pluck('total', 'lokasi');
                if ($paketPerWilayah->count() == 0) {
                    $dataPaket = [];
                }
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->whereMonth('tgl_terima', '=', request()->periode)->where('lokasi', 'Sagalaherang')->pluck('total', 'kelompok');
            if ($paketPerKategori->count() == 0) {
                $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->groupBy('kelompok')->where('lokasi', 'Sagalaherang')->pluck('total', 'kelompok');
                if ($paketPerKategori->count() == 0) {
                    $dataKategori = [];
                }
            }
        } else {

            $kurirPerWilayah = TransaksiPaket::join('ekspedisi', 'ekspedisi.id', '=', 'transaksi_paket.ekspedisi_id')->selectRaw('count(transaksi_paket.no_resi) as total, ekspedisi.name')->where('lokasi', 'Sagalaherang')->groupBy('ekspedisi_id')->pluck('total', 'name');
            if ($kurirPerWilayah->count() == 0) {
                $datakurir = [];
                Alert::error('Oops', 'Data Kurir tidak ditemukan');
            }
            $paketPerWilayah = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, lokasi')->where('lokasi', 'Sagalaherang')->groupBy('lokasi')->pluck('total', 'lokasi');
            if ($kurirPerWilayah->count() == 0) {
                $dataPaket = [];
            }
            $paketPerKategori = TransaksiPaket::selectRaw('count(transaksi_paket.no_resi) as total, kelompok')->where('lokasi', 'Sagalaherang')->groupBy('kelompok')->pluck('total', 'kelompok');
            if ($kurirPerWilayah->count() == 0) {
                $dataKategori = [];
            }
        }

        foreach ($kurirPerWilayah as $index => $kurir) {
            $datakurir[] = [
                "name" => "'$index'",
                "y" => $kurir
            ];
        }
        foreach ($paketPerKategori as $index => $kategori) {
            $dataKategori[] = [
                "name" => "'$index'",
                "y" => $kategori
            ];
        }
        foreach ($paketPerWilayah as $index => $paket) {
            $dataPaket[] = [
                "name" => "'$index'",
                "y" => $paket
            ];
        }

        return view('laporan.grafik', [
            'title' => 'Laporan Grafik',
            'lokasi' => 'Sagalaherang',
            'kurir' => $datakurir,
            'paket' => $dataPaket,
            'kategori' => $dataKategori
        ]);
    }
    public function destroy($id)
    {
        TransaksiPaket::destroy('id', $id);
        return redirect('/laporan')->with('success', 'Laporan');
    }
}
