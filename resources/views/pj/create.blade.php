@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Tambah Penanggun Jawab<h1>
                                        <form class="user" action="/pj" method="post">
                                            @csrf
                                            <div class="form-group row">
                                                <h6 for="bidang" class="col-sm-4">Nama Pegawai</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nama_pegawai"
                                                        id="nama_pegawai">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="no_telepon" class="col-sm-4">No. Telepon</h6>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="no_telepon"
                                                        id="no_telepon">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <h6 for="bidang" class="col-sm-4">Lokasi</h6>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="lokasi_id" name="lokasi_id">
                                                        <option selected disabled>-- Pilih Lokasi --</option>
                                                        @foreach ($lokasi as $l)
                                                            <option value="{{ $l->id }}">{{ $l->nama_lokasi }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/pj" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
