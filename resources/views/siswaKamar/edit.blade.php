@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Add New siswa kamar<h1>
                                        <form class="user" action="/siswa_kamar/{{ $siswa_kamar->id }}" method="post">
                                            @method('put')
                                            @csrf
                                            <div class="form-group">
                                                <h6>Pilih Kamar untuk siswa</h6>
                                                <select class="form-control" id="kamar_id" name="kamar_id">
                                                    <option selected disabled>-- Pilih Kamar --</option>
                                                    @foreach ($kamar as $k)
                                                        @if ($k->id == $siswa_kamar->kamar_id)
                                                            <option value="{{ $k->id }}" selected>
                                                                {{ $k->nama_kamar }}</option>
                                                        @else
                                                            <option value="{{ $k->id }}">{{ $k->nama_kamar }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <h6>Pilih siswa untuk Kamar</h6>
                                                <select class="form-control" id="nis" name="nis">
                                                    <option selected disabled>-- Pilih Siswa --</option>
                                                    @foreach ($siswa as $s)
                                                        @if ($s->nis == $siswa_kamar->nis)
                                                            
                                                        <option value="{{ $s->nis }}" selected>{{ $s->nama_siswa }}</option>
                                                        @else
                                                        <option value="{{ $s->nis }}">{{ $s->nama_siswa }}</option>
                                                            
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/siswa_kamar" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
