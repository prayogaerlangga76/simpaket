<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <img src="{{ asset('assets/img/logo-humas.png') }}" width="40px" height="40px">
        </div>
        <div class="sidebar-brand-text mx-3">Simpaket</div>
    </a>

    <!-- Divider -->
    @if (Auth::user()->role->name == 'Staff')
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
            <a class="nav-link" href="/">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menu Transaksi Paket
        </div>
        <li class="nav-item {{ Request::is('penerimaan') ? 'active' : '' }}">
            <a class="nav-link" href="/penerimaan">
                <i class="fas fa-truck-loading"></i>
                <span>Penerimaan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('pengambilan') ? 'active' : '' }}">
            <a class="nav-link" href="/pengambilan">
                <i class="fas fa-people-carry"></i>
                <span>Pengambilan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('laporan') ? 'active' : '' }}">
            <a class="nav-link" href="/laporan">
                <i class="fas fa-file-contract"></i>
                <span>Laporan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('laporanGrafik') ? 'active' : '' }} ">
            <a class="nav-link " href="/laporanGrafik">
                <i class="fa-solid fa-chart-line"></i>
                <span>Laporan Grafik</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-power-off"></i>
                <span>Logout</span></a>
        </li>
    @endif
    @if (Auth::user()->role->name == 'Administrator')
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
            <a class="nav-link" href="/">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menu Transaksi Paket
        </div>
        <li class="nav-item {{ Request::is('penerimaan') ? 'active' : '' }}">
            <a class="nav-link" href="/penerimaan">
                <i class="fas fa-truck-loading"></i>
                <span>Penerimaan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('pengambilan') ? 'active' : '' }}">
            <a class="nav-link" href="/pengambilan">
                <i class="fas fa-people-carry"></i>
                <span>Pengambilan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('laporan*') ? 'active' : '' }}">
            <a class="nav-link" href="/laporan">
                <i class="fas fa-file-contract"></i>
                <span>Laporan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('laporanGrafik*') ? 'active' : '' }} ">
            <a class="nav-link " href="/laporanGrafik">
                <i class="fa-solid fa-chart-line"></i>
                <span>Laporan Grafik</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menu Administrator
        </div>
        <li class="nav-item">
            <a class="nav-link {{ Request::is('pj') ? 'active' : '' }}" href="/pj">
                <i class="fas fa-users"></i>
                <span>Master PJ</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::is('ltiq') ? 'active' : '' }}" href="/ltiq">
                <i class="fas fa-users"></i>
                <span>Data LTIQ</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::is('ekspedisi') ? 'active' : '' }}" href="/ekspedisi">
                <i class="fas fa-th-large"></i>
                <span>Ekspedisi</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-power-off"></i>
                <span>Logout</span></a>
        </li>
    @endif

    @if (Auth::user()->role->name == 'IT')
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
            <a class="nav-link" href="/">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menu Transaksi Paket
        </div>
        <li class="nav-item {{ Request::is('penerimaan') ? 'active' : '' }}">
            <a class="nav-link" href="/penerimaan">
                <i class="fas fa-truck-loading"></i>
                <span>Penerimaan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('pengambilan') ? 'active' : '' }}">
            <a class="nav-link" href="/pengambilan">
                <i class="fas fa-people-carry"></i>
                <span>Pengambilan Paket</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menu Administrator
        </div>

        <li class="nav-item {{ Request::is('laporanGrafik') ? 'active' : '' }} ">
            <a class="nav-link " href="/laporanGrafik">
                <i class="fa-solid fa-chart-line"></i>
                <span>Laporan Grafik</span></a>
        </li>
        <li class="nav-item {{ Request::is('laporan') ? 'active' : '' }}">
            <a class="nav-link" href="/laporan">
                <i class="fas fa-file-contract"></i>
                <span>Laporan Paket</span></a>
        </li>
        <li class="nav-item {{ Request::is('pj') ? 'active' : '' }}">
            <a class="nav-link" href="/pj">
                <i class="fas fa-users"></i>
                <span>Master PJ</span></a>
        </li>
        <li class="nav-item {{ Request::is('ltiq') ? 'active' : '' }}">
            <a class="nav-link" href="/ltiq">
                <i class="fas fa-users"></i>
                <span>Data LTIQ</span></a>
        </li>
        <li class="nav-item {{ Request::is('ekspedisi') ? 'active' : '' }}">
            <a class="nav-link" href="/ekspedisi">
                <i class="fas fa-th-large"></i>
                <span>Ekspedisi</span></a>
        </li>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="sidebar-heading">
            Menu IT
        </div>
        <li class="nav-item {{ Request::is('lokasi') ? 'active' : '' }}">
            <a class="nav-link " href="/lokasi">
                <i class="fas fa-map"></i>
                <span>Master Lokasi</span></a>
        </li>
        <li class="nav-item {{ Request::is('role') ? 'active' : '' }}">
            <a class="nav-link " href="/role">
                <i class="fas fa-user-tie"></i>
                <span>Role</span></a>
        </li>
        <li class="nav-item {{ Request::is('user') ? 'active' : '' }}">
            <a class="nav-link " href="/user">
                <i class="fas fa-user-check"></i>
                <span>Manajemen User</span></a>
        </li>
        <li class="nav-item {{ Request::is('profile') ? 'active' : '' }}">
            <a class="nav-link" href="/profile/{{ Auth::user()->username }}/edit">
                <i class="fas fa-user"></i>
                <span>Profile</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-power-off"></i>
                <span>Logout</span></a>
        </li>
    @endif
    @if (Auth::user()->role->name == 'Default')
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-power-off"></i>
                <span>Logout</span></a>
        </li>
    @endif
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
