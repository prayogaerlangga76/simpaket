<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function profile(Profile $profile)
    {
        $data = User::where('username', $profile->username)->get();
        return view('user.profile', [
            'title' => 'Update Profile',
            'data' => $data
        ]);
        
    }

    public function updateProfile(Request $request, Profile $profile)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'email' => 'required',
        ]);

        if($request['password']){
            $validatedData['password'] = Hash::make($request['password']);
        }

        User::where('username', $profile->username)->update($validatedData);
        Alert::success('Berhasil', 'Profile berhasil diupdate');
        return redirect('/profile/' . $validatedData['username'] .'/edit');
    }

}
