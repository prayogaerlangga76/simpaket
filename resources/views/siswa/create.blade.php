@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Add New Siswa<h1>
                                        <form class="user" action="/siswa" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <h6>Nis Baru</h6>
                                                <input type="text" class="form-control form-control-user" id="nis"
                                                    name="nis" placeholder="Enter nomor nis .... ">
                                            </div>
                                            <div class="form-group">
                                                <h6>Nama Siswa baru</h6>
                                                <input type="text" class="form-control form-control-user" id="nama_siswa"
                                                    name="nama_siswa" placeholder="Enter nama siswa .... ">
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/siswa" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
