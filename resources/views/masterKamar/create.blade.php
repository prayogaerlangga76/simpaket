@extends('layouts.main')

@section('content')
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <h1 class="h4 text-dark-900 mb-4">Add New pegawai<h1>
                                        <form class="user" action="/kamar" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <h6>Nama Kamar Baru</h6>
                                                <input type="text" class="form-control form-control-user" id="nama_kamar"
                                                    name="nama_kamar" placeholder="Enter Nama kamar .... ">
                                            </div>
                                            <div class="form-group">
                                                <h6>Musyrif/ah</h6>
                                                <select class="form-control" id="pegawai_id" name="pegawai_id">
                                                    <option selected disabled>-- Pilih Pegawai --</option>
                                                    @foreach ($pegawai as $p)
                                                        <option value="{{ $p->id }}">{{ $p->nama_pegawai }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn btn-primary">Save</button>
                                            <a href="/kamar" class="btn btn-secondary">Back</a>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
