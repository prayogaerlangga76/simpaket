@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="mt-5 card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class=" d-flex col-md-5 justify-content-end">
                                <img src="{{ asset('assets\img\logo-assyifa-nobg.png') }}"
                                    style="width:100px; height: 100px">
                            </div>

                            <div class="col-md-5">
                                <br>
                                <h5 class="font-weight-bold text-secondary">LAYANAN PAKET</h5>
                                <h5 class="font-weight-bold text-secondary">AS-SYIFA AL-KHOERIYAH</h5>
                            </div>
                        </div>
                        <div class="container mt-2 border border-secondary pb-5">
                            <div class="row border-bottom">
                                <div class="col-md-6 ">
                                    <h6 class="font-weight-bold mt-4">{{ $judulForm }} Paket</h6>
                                    <h6 class="font-weight-bold">Lokasi : {{ $lokasi }}</h6>

                                </div>
                                <div class="col-md-6 text-right">
                                    <h6 class="badge badge-primary mt-4 text-right">{{ $waktu }}</h6>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <form class="mt-4" action="/pengambilanPaket" method="post">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="no_resi" class="col-sm-4 col-form-label">No. Resi</label>
                                                <div class="col-md-8">
                                                    <select class="form-control js-transaksi-data" 
                                                    id="js-transaksi-data" name="no_resi"></select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kelompok" class="col-sm-4 col-form-label">Status</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="kelompok" id="kelompok" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama_penerima" class="col-sm-4 col-form-label">Nama Penerima</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nama_penerima" id="nama_penerima" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bidang" class="col-sm-4 col-form-label">Bidang/Asrama</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="bidang" id="bidang" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="unit" class="col-sm-4 col-form-label">Unit</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="unit" id="unit" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="no_kamar" class="col-sm-4 col-form-label">No Kamar</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="no_kamar" id="no_kamar" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="keterangan" class="col-sm-4 col-form-label">Nama Keluarga</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="keterangan" id="keterangan" readonly>
                                                </div>
                                            </div>
                                                <input type="hidden" name="lokasi" id="lokasi" value="{{ $lokasi }}">
                                                <input type="hidden" name="waktu" id="waktu" value="{{ $waktu }}">
                                                <input type="hidden" name="id_telegram" id="id_telegram">
                                                <input type="hidden" name="no_uniq" id="no_uniq">
                                                <input type="hidden" name="id" id="id">
                                            <div class="form-group row">
                                                <label for="j_kurir" class="col-md-4">Kurir</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="j_kurir" id="j_kurir"
                                                        readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama_pengirim" class="col-md-4">Nama Pengirim</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nama_pengirim" id="nama_pengirim"
                                                        readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama_pengambil" class="col-sm-4 col-form-label">Nama
                                                    pengambil</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="nama_pengambil" id="nama_pengambil"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary container">Save</button>
                                            <a href="/penerimaan"
                                                class="btn btn-secondary mt-2 d-flex justify-content-center">Back</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function show_form(a) {
            window.location.href = "?op=" + a;
        }
    </script>
@endsection
