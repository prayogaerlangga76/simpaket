<?php

namespace App\Http\Controllers;

use App\Models\Ltiq;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class LtiqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Ltiq::all();
        return view('ltiq.index', [
            'title' => 'Data Dummy LTIQ',
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ltiq.create', [
            'title' => 'Tambah Murid LTIQ',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'unit' => 'required',
            'pegawai_id' => 'required'
        ]);

        Ltiq::create($validatedData);
        Alert::success('Berhasil', 'Data santri atas nama ' . $validatedData['nama'] . ' Berhasil di tambahkan');
        return redirect('/ltiq/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ltiq  $ltiq
     * @return \Illuminate\Http\Response
     */
    public function show(Ltiq $ltiq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ltiq  $ltiq
     * @return \Illuminate\Http\Response
     */
    public function edit(Ltiq $ltiq)
    {
        $data = Ltiq::where('id', $ltiq->id)->first();
        return view('ltiq.edit', [
            'title' => 'Edit Data LTIQ',
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ltiq  $ltiq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ltiq $ltiq)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'unit' => 'required',
        ]);

        if ($request->pegawai_id) {
            $validatedData['pegawai_id'] = $request->pegawai_id;
        }

        Ltiq::where('id', $ltiq->id)->update($validatedData);
        Alert::success('Berhasil', 'Data santri atas nama ' . $validatedData['nama'] . ' Berhasil diperbaharui');
        return redirect('/ltiq');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ltiq  $ltiq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ltiq $ltiq)
    {
        Ltiq::destroy('id', $ltiq->id);
        Alert::success('Berhasil', 'Data santri atas nama ' . $ltiq->nama . ' Berhasil dihapus');
        return redirect('/ltiq');
    }
}
