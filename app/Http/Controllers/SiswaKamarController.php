<?php

namespace App\Http\Controllers;

use App\Models\Kamar;
use App\Models\Siswa;
use App\Models\Siswa_Kamar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiswaKamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa_kamar = DB::table('siswa_kamar')
        ->join('kamar', 'siswa_kamar.kamar_id', '=', 'kamar.id')
        ->join('siswa', 'siswa_kamar.nis', '=', 'siswa.nis')
        ->select('*')
        ->get();
        return view('siswaKamar.index', [
            'title' => 'Daftar Siswa As - syifa',
            'siswa_kamar' => $siswa_kamar,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswaKamar.create', [
            'title' => 'Add new Siswa page',
            'kamar' => Kamar::all(),
            'siswa' => Siswa::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kamar_id' => 'required',
            'nis' => 'required',
        ]);

        Siswa_Kamar::create($validatedData);

        return redirect('/siswa_kamar')->with('success', 'New siswa has been added to new room!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Siswa_Kamar  $siswa_Kamar
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa_Kamar $siswa_Kamar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Siswa_Kamar  $siswa_Kamar
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa_Kamar $siswa_kamar)
    {
        return view('siswaKamar.edit', [
            'title' => 'Edit siswa kamar Page',
            'siswa_kamar' => $siswa_kamar,
            'kamar' => Kamar::all(),
            'siswa' => Siswa::all(),
        ]);

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Siswa_Kamar  $siswa_Kamar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa_Kamar $siswa_kamar)
    {
        $validatedData = $request->validate([
            'kamar_id' => 'required',
            'nis' => 'required',
        ]);
        
        Siswa_Kamar::where('id', $siswa_kamar->id)->update($validatedData);
        return redirect('/siswa_kamar')->with('success', 'Room has been update for room!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Siswa_Kamar  $siswa_Kamar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa_Kamar $siswa_kamar)
    {
        Siswa_Kamar::destroy('id', $siswa_kamar->id);
        return redirect('/siswa_kamar')->with('success', 'siswa has been remove!');
        
    }
}
