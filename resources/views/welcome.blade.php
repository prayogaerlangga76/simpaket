@extends('layouts.main')

@section('content')

    @php
    use Carbon\Carbon;
    $op = '';
    if (isset($_GET['op'])) {
        $op = $_GET['op'];
    }
    @endphp
    <style>
        .big-checkbox {width: 25px; height: 25px; margin-top: 0}
    </style>
    <div class="container-fluid">
        @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Monitoring Paket As - Syifa</h1>
                <div href="#" class="d-none d-sm-inline-block badge badge-primary shadow-sm">{{ $waktu }}</div>
            </div>
            @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
                <div class="container">
                    <div class="row justify-content-center">
                        <button onclick="dLocation('Jalancagak')"
                            class="btn btn-primary mr-1 mb-2 d-flex justify-content-center">Jalancagak</button>
                        <button onclick="dLocation('Wanareja')"
                            class="btn btn-warning mr-1 d-flex mb-2 justify-content-center">Wanareja</button>
                        <button onclick="dLocation('Sagalaherang')"
                            class="btn btn-danger d-flex mb-2 justify-content-center">Sagalaherang</button>
                    </div>
                </div>
            @endif
            @if (request()->op)
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : {{ request()->op }}</h6>
                </div>
            @else
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : {{ Auth::user()->lokasi->nama_lokasi }}</h6>
                </div>
            @endif

            <div id="chartMonth" class="mb-5"></div>
            <!-- Content Row -->
            <div class="row">
                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Paket masuk hari ini</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $dataPerDay }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                        Total paket belum diambil</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $belumAmbil }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai Belum
                                        Ambil
                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $paketPegawai }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pending Requests Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                        Paket Murid Belum Ambil</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $paketSiswa }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Total paket keluarga pegawai belum diambil</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $paketKPegawai }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                        Total paket Murid LTIQ belum diambil</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $paketLtiq }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- @if ((Auth::user()->role_id == 2 && Auth::user()->lokasi_id == 1) || (Auth::user()->role_id == 3 && Auth::user()->lokasi_id == 1)) --}}
            {{-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Wanareja</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            {{ $DLpaketKeluargaPegawai }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Sagalaherang</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai2 }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketKeluargaPegawai2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- @endif --}}
            {{-- @if ((Auth::user()->role_id == 2 && Auth::user()->lokasi_id == 2) || (Auth::user()->role_id == 3 && Auth::user()->lokasi_id == 2))
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Jalancagak</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            {{ $DLpaketKeluargaPegawai }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Sagalaherang</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai2 }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketKeluargaPegawai2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- @endif --}}
            {{-- @if ((Auth::user()->role_id == 2 && Auth::user()->lokasi_id == 3) || (Auth::user()->role_id == 3 && Auth::user()->lokasi_id == 3))
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Jalancagak</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketKeluargaPegawai }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h6 class="h6 mb-0 text-gray-800">Lokasi : Wanareja</h6>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Paket masuk hari ini</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLdataPerDay2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLbelumAmbil2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Paket Pegawai
                                            Belum Ambil
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                    {{ $DLpaketPegawai2 }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Paket Murid Belum Ambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketSiswa2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Total paket keluarga pegawai belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketKeluargaPegawai2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Total paket Murid LTIQ belum diambil</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $DLpaketLtiq2 }}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-people-carry fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}

            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            @if (request()->op)
                                <h6 class="m-0 font-weight-bold text-primary">Rincian Daftar Paket Belum diambil
                                    ({{ request()->op }})</h6>
                            @else
                                <h6 class="m-0 font-weight-bold text-primary">Rincian Daftar Paket Belum diambil
                                    ({{ Auth::user()->lokasi->nama_lokasi }})</h6>
                            @endif
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="pesan"></div>
                            <div class="table-responsive">
                                <table class="table example" id="dataTable">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>No. Resi</td>
                                            <td>Tanggal Terima</td>
                                            <td>Nama Penerima</td>
                                            <td>Nama Pengirim</td>
                                            <td>Status</td>
                                        </tr>
                                    </thead>
                                    @foreach ($paketTersedia as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->no_resi }}</td>
                                            <td>{{ Carbon::parse($item->tgl_terima)->translatedFormat('l, d F Y') }}</td>
                                            <td>{{ $item->nama_penerima }}</td>
                                            <td>{{ $item->nama_pengirim }}</td>
                                            <td>
                                                <div class="form-check d-flex justify-content-center">
                                                    <input class="form-check-input big-checkbox" type="checkbox"
                                                        value="{{ $item->id }}" id="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <h1>Anda Tidak diberikan Akses Apapun</h1>
        @endif
    </div>
    @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
        @push('script')
            <script>
                function dLocation(a) {
                    window.location.href = "?op=" + a;
                }
                
                
            </script>
            <script type="text/javascript">
                var yearly = <?= str_replace("\"", '', json_encode($yearly)) ?>

                Highcharts.chart('chartMonth', {
                    title: {
                        text: 'Data Paket Masuk Perbulan Tahun ' + <?php echo date('Y'); ?>
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    xAxis: {
                        type: 'category',
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah'
                        }

                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true,
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y:,.0f}'
                            }
                        },
                    },
                    series: [{
                        name: 'Total Paket',
                        data: yearly
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }
                });
            </script>
        @endpush
    @endif
@endsection
